# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    This module defines PqosMonitor which is used to monitor events on cores
"""
import signal
import ctypes
import copy
import subprocess
from time import sleep
from pqos_wrapper.cpuinfo import PqosCpuInfo
from pqos_wrapper.utils import (
    wrapper_handle_error,
    convert_units,
    prepare_cmd_output,
    get_core_list,
    Log,
)


class CPqosEventValues(ctypes.Structure):
    """
    pqos_event_values structure
    """

    _fields_ = [
        ("llc", ctypes.c_uint64),
        ("mbm_local", ctypes.c_uint64),
        ("mbm_total", ctypes.c_uint64),
        ("mbm_remote", ctypes.c_uint64),
        ("mbm_local_delta", ctypes.c_uint64),
        ("mbm_total_delta", ctypes.c_uint64),
        ("mbm_remote_delta", ctypes.c_uint64),
        ("ipc_retired", ctypes.c_uint64),
        ("ipc_retired_delta", ctypes.c_uint64),
        ("ipc_unhalted", ctypes.c_uint64),
        ("ipc_unhalted_delta", ctypes.c_uint64),
        ("ipc", ctypes.c_double),
        ("llc_misses", ctypes.c_uint64),
        ("llc_misses_delta", ctypes.c_uint64),
    ]


class CPqosMonPerfctx(ctypes.Structure):
    """
    pqos_mon_perf_ctx structure
    """

    _fields_ = [
        ("fd_llc", ctypes.c_int),
        ("fd_mbl", ctypes.c_int),
        ("fd_mbt", ctypes.c_int),
        ("fd_inst", ctypes.c_int),
        ("fd_cyc", ctypes.c_int),
        ("fd_llc_misses", ctypes.c_int),
    ]


class CPqosMonPollCtx(ctypes.Structure):
    """
    pqos_mon_poll_ctx structure
    """

    _fields_ = [
        ("lcore", ctypes.c_uint),
        ("cluster", ctypes.c_uint),
        ("rmid", ctypes.c_uint32),
    ]


class CPqosMonData(ctypes.Structure):
    """
    pqos_mon_data structure
    """

    _fields_ = [
        ("valid", ctypes.c_int),
        ("event", ctypes.c_int),  # enum pqos_mon_event
        ("context", ctypes.c_void_p),
        ("values", CPqosEventValues),
        ("num_pids", ctypes.c_uint),
        ("pids", ctypes.POINTER(ctypes.c_uint)),  # pid_t
        ("tid_nr", ctypes.c_uint),
        ("tid_map", ctypes.POINTER(ctypes.c_uint)),  # pid_t
        ("perf", ctypes.POINTER(CPqosMonPerfctx)),
        ("perf_event", ctypes.c_int),  # enum pqos_mon_event
        ("resctrl_event", ctypes.c_int),  # enum pqos_mon_event
        ("resctrl_mon_group", ctypes.c_char_p),
        ("resctrl_values_storage", CPqosEventValues),
        ("poll_ctx", ctypes.POINTER(CPqosMonPollCtx)),
        ("num_poll_ctx", ctypes.c_uint),
        ("cores", ctypes.POINTER(ctypes.c_uint)),
        ("num_cores", ctypes.c_uint),
        ("valid_mbm_read", ctypes.c_int),
    ]


class PqosMonitor:
    """
    Class for monitoring events on given sets of cores
    """

    MONITORING_DATA = {
        "cores": {"type": [], "header": "CORES", "unit": None},
        "ipc": {"type": float(0.0), "header": "IPC", "unit": None},
        "llc_misses": {"type": int(0), "header": "MISSES[K]", "unit": "K"},
        "llc": {"type": {"avg": 0.0, "max": 0}, "header": "LLC[KB]", "unit": "KB"},
        "mbm_local": {
            "type": {"avg": 0.0, "max": 0},
            "header": "MBL[MB/s]",
            "unit": "MB/s",
        },
        "mbm_remote": {
            "type": {"avg": 0.0, "max": 0},
            "header": "MBR[MB/s]",
            "unit": "MB/s",
        },
        "mbm_total": {
            "type": {"avg": 0.0, "max": 0},
            "header": "MBT[MB/s]",
            "unit": "MB/s",
        },
    }

    EVENT_ENUMS = {
        1: "llc",
        2: "mbm_local",
        4: "mbm_total",
        8: "mbm_remote",
        16384: "llc_misses",
        32768: "ipc",
    }

    def __init__(self, pqos_lib, output):
        self.lib = pqos_lib
        self.output = output
        self.mon_data = None
        self.monitoring = True
        self.core_list = []
        self.mon_data_avg = []
        self.mon_data_p = []
        self.header = []
        self.header_attr = []
        self.available_mon_events = {}
        self.iterations = 0
        self.cpu_info = PqosCpuInfo(self.lib)

    def monitor_events(self, mon_cap, core_string, time_interval):
        """
        Initialises pqos_mon_data ctypes structures and does initialisation
        for start monitoring pipeline for given cores string

            @mon_cap: The monitoring capability data that includes all the monitoring
            events present in the system
            @core_string: The string representing the list of cores
            to be monitored
            @time_interval: The time interval between two calls for polling
            monitoring data
        """
        self.core_list = get_core_list(core_string)
        self.mon_data = (CPqosMonData * len(self.core_list))()
        pointer_list = []
        for data in self.mon_data:
            pointer_list.append(ctypes.pointer(data))
        self.mon_data_p = (type(pointer_list[0]) * len(pointer_list))(*pointer_list)

        all_evts = self.get_available_events(mon_cap)

        for core_set in self.core_list:
            self.mon_data_avg.append(copy.deepcopy(self.available_mon_events))
            for core in core_set:
                self.cpu_info.check_core(core)

        self.setup_monitoring(all_evts)
        self.monitor_loop(time_interval)
        self.monitor_stop(len(self.core_list))

        ret = {"monitoring_data": self.mon_data_avg}
        return prepare_cmd_output(
            "event monitoring successfull", "monitor_events", **ret
        )

    def get_available_events(self, mon_cap):
        """
        Fetches the available monitoring events from mon_cap and prepares
        header for CLI output and adds data in available_mon_events

            @mon_cap: The get_capability function output for monitoring capability
        """
        ordered_header_attr = [
            "cores",
            "ipc",
            "llc_misses",
            "llc",
            "mbm_remote",
            "mbm_local",
            "mbm_total",
        ]
        all_evts = 0
        self.available_mon_events["cores"] = self.MONITORING_DATA["cores"]["type"]
        for event in mon_cap["events"]:
            all_evts |= event["type"]
            event_name = self.EVENT_ENUMS[event["type"]]
            self.available_mon_events[event_name] = self.MONITORING_DATA[event_name][
                "type"
            ]

        for attr in ordered_header_attr:
            if attr in self.available_mon_events:
                self.header.append(self.MONITORING_DATA[attr]["header"])
                self.header_attr.append(attr)
        return all_evts

    def setup_monitoring(self, all_evts):
        """
        Setup monitoring for all given core sets

            @all_evts: The no that encodes all the monitoring events present
            in the system
        """
        for core_set, index in zip(self.core_list, range(len(self.core_list))):
            cores_array = (ctypes.c_uint * len(core_set))(*core_set)
            ctx = ", ".join(str(core) for core in core_set)
            self.mon_data_avg[index]["cores"] = core_set
            ctx_buffer = ctypes.create_string_buffer(ctx.encode("utf-8"))
            ctx_buffer_cast = ctypes.cast(ctx_buffer, ctypes.c_void_p)
            cores_array_cast = ctypes.cast(cores_array, ctypes.POINTER(ctypes.c_uint))
            ret = self.lib.pqos_mon_start(
                ctypes.c_uint(len(core_set)),
                cores_array_cast,
                ctypes.c_int(all_evts),
                ctx_buffer_cast,
                ctypes.byref(self.mon_data[index]),
            )
            self.monitoring_handle_error(
                "Unable to start monitoring for core set {}".format(core_set),
                ret,
                "pqos_mon_start",
                index,
            )

    def monitoring_handle_error(self, message, ret, function, length):
        """
        Stops monitoring on failing setup or polling data, resets the monitoring
        data for all core sets upto given length.

            @message: The message to be shown if error is detected
            @ret: The return value of pqos library function
            @function: The name of the function for which the error is checked
            @length: The number of core sets for which monitoring has to be stopped
        """
        if ret != Log.PQOS_RETVAL_OK:
            self.monitor_stop(length, True)
            wrapper_handle_error(message, ret, function)

    def monitor_loop(self, time_interval):
        """
        This function runs the main loop to fetch and print monitoring data
        until SIGINT or SIGHUP is received.

            @time_interval: The time interval between two calls for polling
            monitoring data
        """
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGHUP, self.signal_handler)
        while self.monitoring:
            self.poll_monitoring_info()
            self.store_monitoring_info()
            self.print_monitoring_info()
            sleep(time_interval)
            self.iterations += 1
            if self.output:
                subprocess.run("/usr/bin/clear")

    def get_average(self, ind, key):
        """
        Calculates the current average for a specfic monitoring field
        """
        prev_avg = self.mon_data_avg[ind][key]["avg"]
        new_val = getattr(self.mon_data[ind].values, key)
        return ((prev_avg * self.iterations) + new_val) / (self.iterations + 1)

    def store_monitoring_info(self):
        """
        Stores the average and max readings for monitoring data in mon_data_avg
        """
        for ind in range(len(self.core_list)):
            for key in self.mon_data_avg[ind].keys():
                if key != "cores":
                    if isinstance(self.mon_data_avg[ind][key], dict):
                        self.mon_data_avg[ind][key]["max"] = max(
                            self.mon_data_avg[ind][key]["max"],
                            getattr(self.mon_data[ind].values, key),
                        )
                        self.mon_data_avg[ind][key]["avg"] = self.get_average(ind, key)
                    else:
                        self.mon_data_avg[ind][key] = getattr(
                            self.mon_data[ind].values, key
                        )

    def poll_monitoring_info(self):
        """
        This function polls current data using pqos_mon_poll
        """
        ret = self.lib.pqos_mon_poll(
            self.mon_data_p, ctypes.c_uint(len(self.core_list))
        )
        self.monitoring_handle_error(
            "Failed to poll monitoring data", ret, "pqos_mon_poll", len(self.core_list)
        )

    def print_monitoring_info(self):
        """
        Prints the current state of monitoring data for all the core sets if output is enabled
        """
        if self.output:
            fmt = "{:<20}" * len(self.header)
            print(fmt.format(*self.header))  # noqa: T001
            for ind in range(len(self.core_list)):
                output = []
                core_str = ", ".join(str(core) for core in self.core_list[ind])
                output.append(core_str)
                for attr in self.header_attr:
                    if attr != "cores":
                        output.append(
                            convert_units(
                                getattr(self.mon_data[ind].values, attr),
                                self.MONITORING_DATA[attr]["unit"],
                            )
                        )
                print(fmt.format(*output))  # noqa: T001

    def monitor_stop(self, length, suppress_warning=False):
        """
        stop monitoring process upto given number of core sets

            @length: The number of core sets for which monitoring is to be stopped
            @suppress_warning: A boolen to decide if warning has to be raised on failing
            to stop monitoring for a given core set
        """
        for ind in range(length):
            ret = self.lib.pqos_mon_stop(ctypes.byref(self.mon_data[ind]))
            if not suppress_warning:
                wrapper_handle_error(
                    "Unable to stop monitoring process for core set {}".format(
                        self.core_list[ind]
                    ),
                    ret,
                    "pqos_mon_stop",
                )

    def signal_handler(self, signal_num, frame):
        """
        Signal handler for exiting monitoring loop
        """
        self.monitoring = False
