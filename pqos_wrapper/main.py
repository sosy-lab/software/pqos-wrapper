# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    The main module to parse command line arguments and call specific pqos_wrapper API.
"""
import sys
import ctypes
import json
from ctypes.util import find_library
from pqos_wrapper.config import CPqosConfig
from pqos_wrapper.capability import PqosCapability
from pqos_wrapper.allocation import PqosAlloc
from pqos_wrapper.monitor import PqosMonitor
from pqos_wrapper.utils import (
    argument_parser,
    wrapper_handle_error,
    prepare_cmd_output,
    Log,
)


class PqosWrapper(object):
    """
    The Wrapper class to parse arguments and call pqos API

    Attributes:
        RETURN_CODE: The success return code from the CLI
        L3_CDP_CFG_ENUM: Integer to describe l3_cdp_config (any)
        L2_CDP_CFG_ENUM: Integer to describe l2_cdp_config (any)
        MBA_CFG_ENUM: Integer to describe mba_config (any)
    """

    L3_CDP_CFG_ENUM = 2
    L2_CDP_CFG_ENUM = 2
    MBA_CFG_ENUM = 0

    def __init__(self, argv):
        """
        Parse cli arguments and finds PQoS library to construct a new object.

            @argv: the command line arguments passed to the CLI
        """
        self.config = argument_parser(argv[1:])
        libpqos_path = find_library("pqos")
        if not libpqos_path:
            wrapper_handle_error(
                "pqos library initialisation error",
                Log.PQOS_RETVAL_INIT,
                "find_library",
            )
        self.lib = ctypes.cdll.LoadLibrary(libpqos_path)
        self.cap = None

    @classmethod
    def main(cls):
        """
        The entry point for the CLI.
        """
        __instance = cls(sys.argv)
        __instance.execute_cli()
        return Log.PQOS_RETVAL_OK

    def execute_cli(self):
        """
        The pipeline for executing pqos API and print dictionary output
        based on the given arguments of the CLI
        """
        return_dict = {}
        # load pqos library with the given interface
        return_dict["load_pqos"] = self.load_pqos()
        self.load_capabilities()

        # check if capability information is requested
        if self.config["check"] is not None:
            return_dict["check_capability"] = self.check_capability()

        # check if resource allocation is requested
        if self.config["alloc"] is not None:
            return_dict["allocate_resource"] = self.alloc_resource()

        # check if core association and COS information is requested
        if self.config["show"]:
            return_dict["show_association"] = self.show_association()

        # check if complete reset of resources is requested
        if self.config["reset"]:
            return_dict["reset_resources"] = self.reset_resources()

        # check if complete reset of monitoring is requested
        if self.config["reset_mon"]:
            return_dict["reset_monitoring"] = self.reset_monitoring()

        # check if monitoring of events is requested
        if self.config["monitor"] is not None:
            return_dict["monitor_events"] = self.monitor_events()

        self.fini()
        print(json.dumps(return_dict, indent=4))  # noqa: T001

    def load_pqos(self):
        """
        Initializes PQoS library as per the interface provided in command line
        """
        cfg_interface = (
            CPqosConfig.PQOS_INTER_MSR
            if self.config["interface"].upper() == "MSR"
            else CPqosConfig.PQOS_INTER_OS
        )
        config = CPqosConfig(cfg_interface, self.config["verbosity"])
        ret = self.lib.pqos_init(ctypes.byref(config))
        wrapper_handle_error(
            "{} interface initialisation error".format(self.config["interface"]),
            ret,
            "pqos_init",
        )
        return prepare_cmd_output(
            "{} interface intialised".format(self.config["interface"]), "pqos_init"
        )

    def load_capabilities(self):
        """
        Load available pqos capabilities as  PqosCapability object.
        """
        self.cap = PqosCapability(self.lib)

    def check_capability(self):
        """
        Check if the given pqos capability in command line is present in the system
        """
        return self.cap.get_type(self.config["check"])

    def show_association(self):
        """
        Returns core and COS association and COS information for the requested resource
        """
        alloc = PqosAlloc(self.lib, self.cap)
        return alloc.show_association()

    def alloc_resource(self):
        """
        allocate resource (available options l3ca) equally to the sets of cores passed in
        the command line.
        """
        alloc = PqosAlloc(self.lib, self.cap)
        return alloc.allocate_resource(self.config["alloc"][0], self.config["alloc"][1])

    def reset_resources(self):
        """
        Resets configuration of allocation technologies.
        """
        ret = self.lib.pqos_alloc_reset(
            self.L3_CDP_CFG_ENUM, self.L2_CDP_CFG_ENUM, self.MBA_CFG_ENUM
        )
        wrapper_handle_error("Unable to reset resources", ret, "pqos_alloc_reset")
        return prepare_cmd_output("Reset all resources", "pqos_alloc_reset")

    def monitor_events(self):
        """
        monitors events on given sets of cores.
        """
        mon_cap = self.cap.get_type("mon")["function_output"]
        mon = PqosMonitor(self.lib, self.config["mon_output"])
        return mon.monitor_events(
            mon_cap, self.config["monitor"], self.config["time_interval"]
        )

    def reset_monitoring(self):
        """
        Resets RMID for all cores
        """
        ret = self.lib.pqos_mon_reset()
        wrapper_handle_error("Unable to reset monitoring", ret, "pqos_mon_reset")
        return prepare_cmd_output("Reset RMID for all cores", "pqos_mon_reset")

    def fini(self):
        """
        Finalizes PQoS library.
        """
        ret = self.lib.pqos_fini()
        wrapper_handle_error("Unable to finalize pqos library", ret, "pqos_fini")
