# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    The module defines PqosCatL3 which can be used to read or write L3 CAT
    configuration.
"""
import ctypes
from pqos_wrapper.utils import wrapper_handle_error, convert_to_int


class CPqosL3CaMaskCDP(ctypes.Structure):
    "CDP structure from union from pqos_l3ca structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [("data_mask", ctypes.c_uint64), ("code_mask", ctypes.c_uint64)]

    def __init__(self, data_mask=0, code_mask=0):
        self.data_mask = data_mask
        self.code_mask = code_mask


class CPqosL3CaMask(ctypes.Union):
    "Union from pqos_l3ca structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [("ways_mask", ctypes.c_uint64), ("s", CPqosL3CaMaskCDP)]


class CPqosL3Ca(ctypes.Structure):
    "pqos_l3ca structure"

    _fields_ = [
        ("class_id", ctypes.c_uint),
        ("cdp", ctypes.c_int),
        ("u", CPqosL3CaMask),
    ]

    def __init__(self, class_id, cdp, ways_mask):
        super(CPqosL3Ca, self).__init__(class_id=class_id, cdp=cdp)
        self.u.ways_mask = convert_to_int(ways_mask)


class L3COS(object):
    """
    A light COS class which stores class_id and mask only.
    """

    def __init__(self, class_id, mask):
        self.class_id = class_id
        self.mask = mask


class PqosCatL3(object):
    "PQoS L3 Cache Allocation Technology"

    def __init__(self, pqos_lib, cap):
        self.lib = pqos_lib
        self.cap = cap

    def get(self, socket):
        """
        Reads classes of service from a socket.

            @socket: a socket number
        """
        cos_num = self.cap.get_l3ca_cos_num()
        l3cas = (CPqosL3Ca * cos_num)()
        num_ca = ctypes.c_uint(0)
        num_ca_ref = ctypes.byref(num_ca)
        ret = self.lib.pqos_l3ca_get(socket, cos_num, num_ca_ref, l3cas)
        wrapper_handle_error(
            "Unable to get cos information for socket {}".format(socket),
            ret,
            "pqos_l3ca_get",
        )
        coses = [L3COS(l3ca.class_id, l3ca.u.ways_mask) for l3ca in l3cas]
        return coses

    def set(self, socket, coses):  # noqa: A003
        """
        Sets class of service on a specified socket.

            @socket: a socket number
            @coses: a list of PqosCatL3.COS objects, class of service
                   configuration
        """

        pqos_l3_cas = [
            CPqosL3Ca(class_id=cos.class_id, cdp=0, ways_mask=cos.mask) for cos in coses
        ]
        pqos_l3_ca_arr = (CPqosL3Ca * len(pqos_l3_cas))(*pqos_l3_cas)
        ret = self.lib.pqos_l3ca_set(socket, len(pqos_l3_cas), pqos_l3_ca_arr)
        wrapper_handle_error(
            "Unable to set L3 COS for socket {}".format(socket), ret, "pqos_l3ca_set"
        )

    def get_min_cbm_bits(self):
        """
        Gets minimum number of bits which must be set in L3 way mask when
        updating a class of service.
        """

        min_cbm_bits = ctypes.c_uint(0)
        min_cbm_bits_ref = ctypes.byref(min_cbm_bits)
        ret = self.lib.pqos_l3ca_get_min_cbm_bits(min_cbm_bits_ref)
        wrapper_handle_error(
            "Unable to get minimum cbm bits", ret, "pqos_l3ca_get_min_cbm_bits"
        )
        return min_cbm_bits.value
