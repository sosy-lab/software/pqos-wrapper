# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
The module defines PqosCap which can be used to read PQoS capabilities.
"""

import ctypes
from pqos_wrapper.utils import wrapper_handle_error, prepare_cmd_output, Log


class CPqosCapabilityL3(ctypes.Structure):
    "pqos_cap_l3ca structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [
        ("mem_size", ctypes.c_uint),
        ("num_classes", ctypes.c_uint),
        ("num_ways", ctypes.c_uint),
        ("way_size", ctypes.c_uint),
        ("way_contention", ctypes.c_uint64),
        ("cdp", ctypes.c_int),
        ("cdp_on", ctypes.c_int),
    ]


class CPqosCapabilityL2(ctypes.Structure):
    "pqos_cap_l2ca structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [
        ("mem_size", ctypes.c_uint),
        ("num_classes", ctypes.c_uint),
        ("num_ways", ctypes.c_uint),
        ("way_size", ctypes.c_uint),
        ("way_contention", ctypes.c_uint64),
        ("cdp", ctypes.c_int),
        ("cdp_on", ctypes.c_int),
    ]


class CPqosCapabilityMBA(ctypes.Structure):
    "pqos_cap_mba structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [
        ("mem_size", ctypes.c_uint),
        ("num_classes", ctypes.c_uint),
        ("throttle_max", ctypes.c_uint),
        ("throttle_step", ctypes.c_uint),
        ("is_linear", ctypes.c_int),
        ("ctrl", ctypes.c_int),
        ("ctrl_on", ctypes.c_int),
    ]


class CPqosMonitor(ctypes.Structure):
    "pqos_monitor structure"
    # pylint: disable=too-few-public-methods

    PQOS_MON_EVENT_L3_OCCUP = 1
    PQOS_MON_EVENT_LMEM_BW = 2
    PQOS_MON_EVENT_TMEM_BW = 4
    PQOS_MON_EVENT_RMEM_BW = 8
    RESERVED1 = 0x1000
    RESERVED2 = 0x2000
    PQOS_PERF_EVENT_LLC_MISS = 0x4000
    PQOS_PERF_EVENT_IPC = 0x8000

    _fields_ = [
        ("type", ctypes.c_int),
        ("max_rmid", ctypes.c_uint),
        ("scale_factor", ctypes.c_uint32),
    ]


class CPqosCapabilityMonitoring(ctypes.Structure):
    "pqos_cap_mon structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [
        ("mem_size", ctypes.c_uint),
        ("max_rmid", ctypes.c_uint),
        ("l3_size", ctypes.c_uint),
        ("num_events", ctypes.c_uint),
        ("events", CPqosMonitor * 0),
    ]


class CPqosCapabilityUnion(ctypes.Union):
    "Union from pqos_capability structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [
        ("mon", ctypes.POINTER(CPqosCapabilityMonitoring)),
        ("l3ca", ctypes.POINTER(CPqosCapabilityL3)),
        ("l2ca", ctypes.POINTER(CPqosCapabilityL2)),
        ("mba", ctypes.POINTER(CPqosCapabilityMBA)),
        ("generic_ptr", ctypes.c_void_p),
    ]


class CPqosCapability(ctypes.Structure):
    "pqos_capability structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [("type", ctypes.c_int), ("u", CPqosCapabilityUnion)]


class CPqosCap(ctypes.Structure):
    "pqos_cap structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [
        ("mem_size", ctypes.c_uint),
        ("version", ctypes.c_uint),
        ("num_cap", ctypes.c_uint),
        ("capabilities", CPqosCapability * 0),
    ]


class PqosCapability(object):
    """
    This class is used to retrieve capabilities from pqos.
    """

    def __init__(self, pqos_lib):
        """
        Initialise capabilities from pqos.

            @pqos_lib: The pqos library instance.
        """
        self.lib = pqos_lib
        self.p_cap = ctypes.POINTER(CPqosCap)()
        ret = self.lib.pqos_cap_get(ctypes.byref(self.p_cap), None)
        wrapper_handle_error(
            "Could not initialise capabilities from pqos", ret, "pqos_cap_get"
        )

    def get_type(self, __type):
        """
        Retrieves the given capability from pqos capabilities object.

            @__type: The name of the capability,
                     Available options: l3ca
        """
        p_cap_item = ctypes.POINTER(CPqosCapability)()
        ret = self.lib.pqos_cap_get_type(
            self.p_cap,
            self.available_capabilities(__type.lower()),
            ctypes.byref(p_cap_item),
        )
        wrapper_handle_error(
            "Failed to retrieve {} capability".format(__type.lower()),
            ret,
            "pqos_cap_get_type",
        )
        cap_item = p_cap_item.contents
        capability = self.get_capability_info(cap_item, __type)
        return prepare_cmd_output(
            "Retrieved {} capability".format(__type.lower()),
            "get_capability_info",
            **capability
        )

    @staticmethod
    def available_capabilities(__type):
        """
        Check if given capability is available in system and return
        its enum code.

            @__type: The name of the capability.
        """
        available_capabilities = {"mon": 0, "l3ca": 1, "l2ca": 2, "mba": 3}
        if __type not in available_capabilities.keys():
            wrapper_handle_error(
                "Invalid capability requested, available options: {}".format(
                    ", ".join(list(available_capabilities.keys()))
                ),
                Log.PQOS_RETVAL_PARAM,
                "available_capabilities",
            )
        return available_capabilities[__type]

    def get_capability_info(self, cap_item, __type):
        """
        Get the information of the requested capability from capabilites object

            @cap_item: CPqosCapability object containing capability information.
            @__type: The name of capability requested.
        """
        capability_info = {}
        cap_object = getattr(cap_item.u, __type.lower()).contents
        for field in cap_object._fields_:
            capability_info[field[0]] = self.get_value(
                __type.lower(), cap_object, field[0]
            )
        return capability_info

    @staticmethod
    def get_value(__type, cap_object, attribute):
        """
        function to get a value from a struct field and convert the array
        of CpqosMonitor structs in CpqosMonitoring events field as an array of dicts.

            @__type: The name of capability requested
            @cap_object: The corresponding ctypes struct containing information
            @attribute: The attribute in cap_object whose value is requested
        """
        if __type == "mon" and attribute == "events":
            events_array = (CPqosMonitor * cap_object.num_events).from_address(
                ctypes.addressof(cap_object.events)
            )
            events_list = []
            for event in events_array:
                events_list.append({})
                for field in event._fields_:
                    events_list[-1][field[0]] = getattr(event, field[0])
            return events_list
        else:
            return getattr(cap_object, attribute)

    def get_l3ca_cos_num(self):
        """
        Retrieves number of L3 allocation classes of service from
        a cap structure.
        """
        cos_num = ctypes.c_uint(0)
        ret = self.lib.pqos_l3ca_get_cos_num(self.p_cap, ctypes.byref(cos_num))
        wrapper_handle_error(
            "Unable to get the no of L3 allocation COS", ret, "pqos_l3ca_get_cos_num"
        )
        return cos_num.value
