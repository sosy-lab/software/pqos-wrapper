# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    This module contains functions for miscellaneous utilities required by the pqos_wrapper CLI
"""

import argparse
import sys
import json
from importlib import import_module


class Log(object):
    """
    Class to store various error codes and their significance
    """

    # pylint: disable=too-few-public-methods

    PQOS_RETVAL_OK = 0  # everything OK
    PQOS_RETVAL_ERROR = 1  # generic error
    PQOS_RETVAL_PARAM = 2  # parameter error
    PQOS_RETVAL_RESOURCE = 3  # resource error
    PQOS_RETVAL_INIT = 4  # initialization error
    PQOS_RETVAL_TRANSPORT = 5  # transport error
    PQOS_RETVAL_PERF_CTR = 6  # performance counter error
    PQOS_RETVAL_BUSY = 7  # resource busy error
    PQOS_RETVAL_INTER = 8  # Interface not supported


def argument_parser(argv):
    """
    Create an argument parser for pqos_wrapper CLI and parse the given arguments.

        @argv: the command line arguments passed to the CLI
    """
    parser = argparse.ArgumentParser(
        description="Execute pqos library functions using command line"
    )
    parser.add_argument(
        "-I",
        "--interface",
        dest="interface",
        default="MSR",
        action="store_const",
        const="OS",
        help="Initialise pqos library using the given interface",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbosity",
        default=-1,
        action="store_const",
        const=1,
        help="Set the verbosity level of output",
    )
    parser.add_argument(
        "-c",
        "--check",
        dest="check",
        help="Check if given capability is present in the system",
    )
    parser.add_argument(
        "-a",
        "--alloc",
        dest="alloc",
        nargs=2,
        help="allocate given resource to listed sets of cores",
    )
    parser.add_argument(
        "-m",
        "--monitor",
        dest="monitor",
        help="monitor all events on given sets of cores",
    )
    parser.add_argument(
        "-t",
        "--time-interval",
        dest="time_interval",
        type=positive_float,
        default=0.1,
        help="set time interval between two calls for polling monitoring data",
    )
    parser.add_argument(
        "-o",
        "--output",
        dest="mon_output",
        default=False,
        action="store_true",
        help="Print monitoring data on stdout",
    )
    parser.add_argument(
        "-s",
        "--show",
        dest="show",
        default=False,
        action="store_true",
        help="show core association and information about the given resource",
    )
    parser.add_argument(
        "-r",
        "--reset",
        dest="reset",
        action="store_true",
        help="Reset configuration of allocation technologies",
    )
    parser.add_argument(
        "-rm",
        "--resetmon",
        dest="reset_mon",
        default=False,
        action="store_true",
        help="Reset RMID to 0 for all cores",
    )
    return vars(parser.parse_args(argv))


def wrapper_handle_error(message, returncode, function, expected=Log.PQOS_RETVAL_OK):
    """
    Raise error in the form of python dict and exit the CLI.

        @message: The error message to be printed.
        @returncode: The status code raised by the error.
        @function: The name of the function raising error.
        @expected: The expected return code from the function.
    """
    if returncode != expected:
        sys.exit(
            json.dumps(
                prepare_cmd_output(message, function, error=True, returncode=returncode)
            )
        )


def prepare_cmd_output(
    message, function, error=False, returncode=Log.PQOS_RETVAL_OK, **kwargs
):
    """
    Prepare a cmd_output for given parameters in the form of a python dict.

        @message: The message to be printed on the CLI.
        @function: The name of the function whose output is being recorded.
        @error: Boolean value to check if output is an error message.
        @returncode: The value returned by the function.
    """
    cmd_output = {
        "function": function,
        "message": message,
        "returncode": returncode,
        "error": error,
        "function_output": kwargs,
    }
    return cmd_output


def load_dynamic(module_name, class_name, *args):
    """
    dynamically load an instance of a class from a module.

        @module_name: name of the module to be imported.
        @class_name: name of the class to be imported.
    """
    module = import_module(module_name)
    return getattr(module, class_name)(*args)


def get_core_list(core_string):
    """
    Convert a string in a list format to a python list.

        @core_string: The input string representing a python list
    """
    try:
        core_list = json.loads(core_string)
        assert (
            all(type(obj) == list for obj in core_list)
            and all(len(obj) > 0 for obj in core_list)
            and type(core_list) == list
            and len(core_list) > 0
        )
    except json.decoder.JSONDecodeError:
        wrapper_handle_error(
            "Incorrect core list format", Log.PQOS_RETVAL_PARAM, "get_core_list"
        )
    except AssertionError:
        wrapper_handle_error(
            "The individual core set should be in list format",
            Log.PQOS_RETVAL_PARAM,
            "get_core_list",
        )
    except TypeError:
        wrapper_handle_error(
            "core_string should be a str, bytes or bytearray",
            Log.PQOS_RETVAL_PARAM,
            "get_core_list",
        )
    return core_list


def convert_to_int(mask):
    """
    Returns a bitmask in the form of an integer.

        @mask: The bitmask in the form of an int or a hex string.
    """
    if isinstance(mask, type(str)):
        return int(mask.lower(), 16)
    elif isinstance(mask, int):
        return mask
    return Log.PQOS_RETVAL_OK


def convert_units(value, unit):
    """
    Convert the value to the respective measuring unit

        @value: The value to be converted
        @unit: The unit of measurement
    """
    if isinstance(unit, str):
        if unit.startswith("K"):
            ret = value / 1000.0
        elif unit.startswith("M"):
            ret = value / (1000.0 * 1000.0)
        else:
            ret = value
    else:
        ret = value
    return ret


def positive_float(x):
    """
    helper function for inputting time interval from cli
    """
    val = float(x)
    if val <= 0.0:
        raise argparse.ArgumentTypeError("time interval should be > 0")
    return val
