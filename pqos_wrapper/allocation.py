# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    The module defines PqosAlloc which is used to associate allocation classes
    of service to logical cores and distribute resources between logical cores.
"""
import ctypes
from pqos_wrapper.utils import (
    get_core_list,
    wrapper_handle_error,
    prepare_cmd_output,
    Log,
)
from pqos_wrapper.l3ca import PqosCatL3, L3COS
from pqos_wrapper.cpuinfo import PqosCpuInfo, CPqosCoreInfo


class PqosAlloc(object):
    """
    Handles associations between allocation classes of service and logical
    cores and allocation of resources.
    """

    def __init__(self, pqos_lib, cap):
        self.lib = pqos_lib
        self.cap = cap
        self.cpu_info = PqosCpuInfo(self.lib)
        self.cores = []
        self.ret = {}

    def allocate_resource(self, resource_type, core_string):
        """
        Allocate requested resource (available options l3ca) between the listed cores.

            @resource_type: The name of the resource to be allocated
            @core_string: A string describing the list of cores for allocation
        """
        core_list = get_core_list(core_string)
        self.get_core_info(core_list)

        if resource_type.lower() == "l3ca":
            self.allocate_l3ca()
        else:
            wrapper_handle_error(
                "Unidentified resource type provided",
                Log.PQOS_RETVAL_PARAM,
                "allocate_resource",
            )
        return prepare_cmd_output(
            "resource allocation successfull", "allocate_resource", **self.ret
        )

    def show_association(self):
        """
        Returns the association between cores and corresponging class of service,
        And also returns the COS information for each socket.
        """
        self.ret = {"cores": {}, "sockets": {}}
        num_cores = self.cpu_info.p_cpu.contents.num_cores
        core_array = (CPqosCoreInfo * num_cores).from_address(
            ctypes.addressof(self.cpu_info.p_cpu.contents.cores)
        )
        for core in core_array:
            self.ret["cores"][core.lcore] = self.assoc_get(core.lcore)

        sockets = self.cpu_info.get_sockets()
        l3ca = PqosCatL3(self.lib, self.cap)
        for socket in sockets:
            cos_info = l3ca.get(socket)
            socket_info = {}
            for cos in cos_info:
                socket_info[cos.class_id] = cos.mask
            self.ret["sockets"][socket] = socket_info
        return prepare_cmd_output(
            "Successfully retrieved core and COS associations",
            "show_association",
            **self.ret
        )

    def get_core_info(self, core_list):
        """
        Get core information for all the cores given in the core_list.

            @core_list: A list containing list of cores, each list containing the cores allocated
                        for each core_set.
        """
        for core_set in core_list:
            core_info = {}
            for core in core_set:
                core_info[core] = self.cpu_info.get_core_info(core)
            self.cores.append(core_info)

    def assoc_set(self, core, class_id):
        """
        Associates a logical core with a given class of service.

            @core: a logical core number
            @class_id: class of service
        """
        ret = self.lib.pqos_alloc_assoc_set(core, class_id)
        wrapper_handle_error(
            "Unable to set COS {0} for core {1}".format(class_id, core),
            ret,
            "pqos_alloc_assoc_set",
        )

    def assoc_get(self, core):
        """
        Gets the class id associated with a core

            @core: a logical core number
        """
        class_id = ctypes.c_uint(0)
        ret = self.lib.pqos_alloc_assoc_get(core, ctypes.byref(class_id))
        wrapper_handle_error(
            "Unable to get class id for core {}".format(core),
            ret,
            "pqos_alloc_assoc_get",
        )
        return class_id.value

    def get_l3ca_info(self):
        """
        Get the cache per run and socket and COS info for l3ca allocation.
        """
        ret = {}
        ret["cos_num"] = self.cap.get_l3ca_cos_num()
        sockets = self.cpu_info.get_sockets()
        l3ca = PqosCatL3(self.lib, self.cap)
        min_cbm_bits = l3ca.get_min_cbm_bits()
        cache_bitmask_length = self.cap.get_type("l3ca")["function_output"]["num_ways"]
        ret["socket_cos_map"] = {socket_id: ret["cos_num"] for socket_id in sockets}
        if len(self.cores) == 0:
            wrapper_handle_error(
                "Core list empty, please add the list of cores for resource allocation",
                Log.PQOS_RETVAL_RESOURCE,
                "get_l3ca_info",
            )
        ret["cache_per_run"] = int(cache_bitmask_length // len(self.cores))
        if min_cbm_bits > ret["cache_per_run"]:
            wrapper_handle_error(
                "Minimum cache capacity reached, reduce the no of runs to isolate cache",
                Log.PQOS_RETVAL_RESOURCE,
                "get_l3ca_info",
            )
        ret["cache_bitmask"] = (1 << ret["cache_per_run"]) - 1
        return ret

    def allocate_l3ca(self):
        """
        Allocate L3 cache equally among core sets using L3 CAT
        """
        l3ca = PqosCatL3(self.lib, self.cap)
        l3ca_info = self.get_l3ca_info()
        self.ret = {"cores": {}, "sockets": {}}
        self.ret["cache_per_run"] = l3ca_info["cache_per_run"]
        for core_set, index in zip(self.cores, range(len(self.cores))):
            sockets_for_core_set = set()
            for core in core_set.keys():
                cos_no = (
                    l3ca_info["cos_num"]
                    - l3ca_info["socket_cos_map"][core_set[core].socket]
                )
                self.assoc_set(core, cos_no)
                self.ret["cores"][core] = cos_no
                sockets_for_core_set.add(core_set[core].socket)
            for socket in sockets_for_core_set:
                class_id = l3ca_info["cos_num"] - l3ca_info["socket_cos_map"][socket]
                mask = l3ca_info["cache_bitmask"] << (
                    (l3ca_info["cache_per_run"]) * index
                )
                cos = L3COS(class_id, mask)
                if socket not in self.ret["sockets"]:
                    self.ret["sockets"][socket] = {}
                self.ret["sockets"][socket][class_id] = mask
                l3ca.set(socket, [cos])
                l3ca_info["socket_cos_map"][socket] -= 1
