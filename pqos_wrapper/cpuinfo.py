# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    This module defines PqosCpuInfo which is to get CPU information
"""
import ctypes
from pqos_wrapper.utils import wrapper_handle_error, Log


class CPqosCoreInfo(ctypes.Structure):
    "pqos_coreinfo structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [
        ("lcore", ctypes.c_uint),  # Logical core id
        ("socket", ctypes.c_uint),  # Socket id in the system
        ("l3_id", ctypes.c_uint),  # L3/LLC cluster id
        ("l2_id", ctypes.c_uint),  # L2 cluster id
    ]


class CPqosCacheInfo(ctypes.Structure):
    "pqos_cacheinfo structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [
        ("detected", ctypes.c_int),  # Indicates cache detected & valid
        ("num_ways", ctypes.c_uint),  # Number of cache ways
        ("num_sets", ctypes.c_uint),  # Number of sets
        ("num_partitions", ctypes.c_uint),  # Number of partitions
        ("line_size", ctypes.c_uint),  # Cache line size in bytes
        ("total_size", ctypes.c_uint),  # Total cache size in bytes
        ("way_size", ctypes.c_uint),  # Cache way size in bytes
    ]


class CPqosCpuInfo(ctypes.Structure):
    "pqos_cpuinfo structure"
    # pylint: disable=too-few-public-methods

    _fields_ = [
        ("mem_size", ctypes.c_uint),  # Byte size of the structure
        ("l2", CPqosCacheInfo),  # L2 cache information
        ("l3", CPqosCacheInfo),  # L3 cache information
        ("num_cores", ctypes.c_uint),  # Number of cores in the system
        ("cores", CPqosCoreInfo * 0),  # Core information
    ]


class PqosCoreInfo(object):
    "Core information"
    # pylint: disable=too-few-public-methods

    def __init__(self, core, socket, l3_id, l2_id):
        self.core = core
        self.socket = socket
        self.l3_id = l3_id
        self.l2_id = l2_id


class PqosCpuInfo(object):
    """
    This class defines functions to get CPU information
    """

    def __init__(self, pqos_lib):
        self.lib = pqos_lib
        self.p_cpu = ctypes.POINTER(CPqosCpuInfo)()
        ret = self.lib.pqos_cap_get(None, ctypes.byref(self.p_cpu))
        wrapper_handle_error("Unable to get cpu information", ret, "pqos_cap_get")

    def get_sockets(self):
        """
        Retrieves socket IDs from CPU info structure.
        """
        count = ctypes.c_uint(0)
        count_ref = ctypes.byref(count)
        self.lib.pqos_cpu_get_sockets.restype = ctypes.POINTER(ctypes.c_uint)
        socket_array = self.lib.pqos_cpu_get_sockets(self.p_cpu, count_ref)
        socket_list = (
            [socket_array[i] for i in range(count.value)] if count.value else []
        )
        return socket_list

    def get_core_info(self, core_id):
        """
        Retrieves core information from CPU info structure for a core.

            @core_id: core ID
        """

        restype = ctypes.POINTER(CPqosCoreInfo)
        self.lib.pqos_cpu_get_core_info.restype = restype
        p_coreinfo = self.lib.pqos_cpu_get_core_info(self.p_cpu, core_id)

        if not p_coreinfo:
            wrapper_handle_error(
                "Core information not found for core {}".format(core_id),
                Log.PQOS_RETVAL_ERROR,
                "pqos_cpu_get_core_info",
            )

        coreinfo_struct = p_coreinfo.contents
        coreinfo = PqosCoreInfo(
            core=coreinfo_struct.lcore,
            socket=coreinfo_struct.socket,
            l3_id=coreinfo_struct.l3_id,
            l2_id=coreinfo_struct.l2_id,
        )
        return coreinfo

    def check_core(self, core):
        """
        Verifies if a specified core is a valid logical core ID.

            @core: core ID
        """

        ret = self.lib.pqos_cpu_check_core(self.p_cpu, core)
        wrapper_handle_error("Invalid core id passed", ret, "pqos_cpu_check_core")
