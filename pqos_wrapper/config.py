# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    This module define the pqos config structure which is used to load pqos library
"""
import ctypes


class CPqosConfig(ctypes.Structure):
    """
    pqos_config structure
    """

    # pylint: disable=too-few-public-methods

    PQOS_INTER_MSR = 0
    PQOS_INTER_OS = 1

    LOG_VER_VERBOSE = 1
    LOG_VER_SILENT = -1

    LOG_CALLBACK = ctypes.CFUNCTYPE(
        None, ctypes.c_void_p, ctypes.c_size_t, ctypes.c_char_p
    )

    _fields_ = [
        ("fd_log", ctypes.c_int),
        ("callback_log", LOG_CALLBACK),
        ("context_log", ctypes.c_void_p),
        ("verbose", ctypes.c_int),
        ("interface", ctypes.c_int),
    ]

    def __init__(
        self,
        interface,
        verbose,
        fd_log=0,
        callback_log=LOG_CALLBACK(0),  # noqa: B008 safe for ctypes
        context_log=None,
    ):
        super(CPqosConfig, self).__init__(
            fd_log, callback_log, context_log, verbose, interface
        )
