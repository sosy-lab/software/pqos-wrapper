# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    Unit tests for main module and integration tests
"""
import ctypes
import json
import unittest
import sys
from io import StringIO
from unittest.mock import patch, MagicMock
from pqos_wrapper.main import PqosWrapper
from pqos_wrapper.utils import Log
from test.mock_pqos_cap import PqosCapMockBuilder
from test.mock_pqos_cpuinfo import PqosCpuMockBuilder
from test.mock_pqos_main import (
    mock_pqos_lib_basic,
    mock_pqos_lib_error_load,
    mock_pqos_lib_error_fini,
    mock_pqos_lib_error_reset,
    mock_pqos_lib_integrated,
)


class TestMain(unittest.TestCase):
    """
    Testing class for main module with command line tests
    """

    @patch("pqos_wrapper.main.find_library", return_value="/path/to/pqos/lib")
    @patch("pqos_wrapper.main.ctypes.cdll.LoadLibrary", side_effect=mock_pqos_lib_basic)
    def test_main_init(self, mock_load_lib, mock_find_lib):
        """
        Test initialisation of PqosWrapper
        """
        wrapper = PqosWrapper(["pqos_wrapper"])
        self.assertIsInstance(wrapper, PqosWrapper)

    @patch("pqos_wrapper.main.find_library", return_value=None)
    @patch("pqos_wrapper.main.ctypes.cdll.LoadLibrary", side_effect=mock_pqos_lib_basic)
    def test_main_init_error(self, mock_load_lib, mock_find_lib):
        """
        Test error raise on failing initialisation
        """
        with self.assertRaises(SystemExit) as err:
            PqosWrapper(["pqos_wrapper"])

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "find_library")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_INIT)
        self.assertEqual(error_json["error"], True)

    @patch("pqos_wrapper.main.find_library", return_value="/path/to/pqos/lib")
    @patch("pqos_wrapper.main.ctypes.cdll.LoadLibrary", side_effect=mock_pqos_lib_basic)
    def test_main_load_pqos(self, mock_load_lib, mock_find_lib):
        """
        Test load_pqos function
        """
        wrapper = PqosWrapper(["pqos_wrapper"])
        ret = wrapper.load_pqos()
        self.assertEqual(ret["error"], False)
        self.assertEqual(ret["returncode"], 0)

    @patch("pqos_wrapper.main.find_library", return_value="/path/to/pqos/lib")
    @patch(
        "pqos_wrapper.main.ctypes.cdll.LoadLibrary",
        side_effect=mock_pqos_lib_error_load,
    )
    def test_main_load_pqos_error(self, mock_load_lib, mock_find_lib):
        """
        Check error on unable to load pqos
        """
        wrapper = PqosWrapper(["pqos_wrapper"])
        with self.assertRaises(SystemExit) as err:
            wrapper.load_pqos()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_init")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)

    @patch("pqos_wrapper.main.find_library", return_value="/path/to/pqos/lib")
    @patch("pqos_wrapper.main.ctypes.cdll.LoadLibrary", side_effect=mock_pqos_lib_basic)
    def test_main_reset_resources(self, mock_load_lib, mock_find_lib):
        """
        Test reset_resources function
        """
        wrapper = PqosWrapper(["pqos_wrapper"])
        ret = wrapper.reset_resources()
        self.assertEqual(ret["error"], False)
        self.assertEqual(ret["returncode"], 0)

    @patch("pqos_wrapper.main.find_library", return_value="/path/to/pqos/lib")
    @patch(
        "pqos_wrapper.main.ctypes.cdll.LoadLibrary",
        side_effect=mock_pqos_lib_error_reset,
    )
    def test_main_reset_resources_error(self, mock_load_lib, mock_find_lib):
        """
        Check error on unable to load pqos
        """
        wrapper = PqosWrapper(["pqos_wrapper"])
        with self.assertRaises(SystemExit) as err:
            wrapper.reset_resources()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_alloc_reset")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)

    @patch("pqos_wrapper.main.find_library", return_value="/path/to/pqos/lib")
    @patch("pqos_wrapper.main.ctypes.cdll.LoadLibrary", side_effect=mock_pqos_lib_basic)
    def test_main_fini(self, mock_load_lib, mock_find_lib):
        """
        Test reset_resources function
        """
        wrapper = PqosWrapper(["pqos_wrapper"])
        wrapper.fini()

    @patch("pqos_wrapper.main.find_library", return_value="/path/to/pqos/lib")
    @patch(
        "pqos_wrapper.main.ctypes.cdll.LoadLibrary",
        side_effect=mock_pqos_lib_error_fini,
    )
    def test_main_fini_error(self, mock_load_lib, mock_find_lib):
        """
        Check error on unable to load pqos
        """
        wrapper = PqosWrapper(["pqos_wrapper"])
        with self.assertRaises(SystemExit) as err:
            wrapper.fini()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_fini")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)


@patch("pqos_wrapper.main.find_library", return_value="/path/to/pqos/lib")
@patch(
    "pqos_wrapper.main.ctypes.cdll.LoadLibrary", side_effect=mock_pqos_lib_integrated
)
class TestMainCli(unittest.TestCase):
    """
    Test command line instruction on pqos_wrapper
    """

    def setUp(self):
        self.stdout = sys.stdout
        self.result_string = StringIO()
        sys.stdout = self.result_string

    def tearDown(self):
        sys.stdout = self.stdout

    def test_cli_init(self, mock_load_lib, mock_find_lib):
        """
        Test initialisation of pqos using cli
        """
        mock_cap = PqosCapMockBuilder()
        mock_cap.append_to_capability_array("l3ca")
        mock_cap.append_to_capability_array("l2ca")
        p_cap = mock_cap.build_cap()

        mock_cpu = PqosCpuMockBuilder()
        p_cpu = mock_cpu.build_cpu(8)

        def pqos_cap_get_mock(cap, cpu):
            """
            mock pqos_cap_get function
            """
            if cpu:
                ctypes.memmove(cpu, ctypes.addressof(p_cpu), ctypes.sizeof(p_cpu))
            if cap:
                ctypes.memmove(cap, ctypes.addressof(p_cap), ctypes.sizeof(p_cap))
            return Log.PQOS_RETVAL_OK

        wrapper = PqosWrapper(["pqos_wrapper"])
        wrapper.lib.pqos_cap_get = MagicMock(side_effect=pqos_cap_get_mock)
        wrapper.execute_cli()
        self.result_string.seek(0)
        result_json = json.load(self.result_string)
        self.assertEqual(result_json["load_pqos"]["function"], "pqos_init")
        self.assertEqual(result_json["load_pqos"]["error"], False)
        self.assertEqual(result_json["load_pqos"]["returncode"], Log.PQOS_RETVAL_OK)

    def test_cli_get_capability_l3ca(self, mock_load_lib, mock_find_lib):
        """
        Test checking l3ca capability pqos using cli
        """
        mock_cap = PqosCapMockBuilder()
        mock_cap.append_to_capability_array("l3ca")
        mock_cap.append_to_capability_array("l2ca")
        p_cap = mock_cap.build_cap()

        mock_cpu = PqosCpuMockBuilder()
        p_cpu = mock_cpu.build_cpu(8)

        def pqos_cap_get_mock(cap, cpu):
            """
            mock pqos_cap_get function
            """
            if cpu:
                ctypes.memmove(cpu, ctypes.addressof(p_cpu), ctypes.sizeof(p_cpu))
            if cap:
                ctypes.memmove(cap, ctypes.addressof(p_cap), ctypes.sizeof(p_cap))
            return Log.PQOS_RETVAL_OK

        wrapper = PqosWrapper(["pqos_wrapper", "-c", "l3ca"])
        wrapper.lib.pqos_cap_get = MagicMock(side_effect=pqos_cap_get_mock)
        wrapper.execute_cli()
        self.result_string.seek(0)
        result_json = json.load(self.result_string)
        self.assertEqual(
            result_json["check_capability"]["function"], "get_capability_info"
        )
        self.assertEqual(result_json["check_capability"]["error"], False)
        self.assertEqual(
            result_json["check_capability"]["returncode"], Log.PQOS_RETVAL_OK
        )
        l3ca = result_json["check_capability"]["function_output"]
        mock_l3ca = PqosCapMockBuilder.build_l3ca_capability()
        self.assertEqual(l3ca["mem_size"], mock_l3ca.mem_size)
        self.assertEqual(l3ca["num_classes"], mock_l3ca.num_classes)
        self.assertEqual(l3ca["num_ways"], mock_l3ca.num_ways)
        self.assertEqual(l3ca["way_size"], mock_l3ca.way_size)
        self.assertEqual(l3ca["way_contention"], mock_l3ca.way_contention)
        self.assertEqual(l3ca["cdp"], mock_l3ca.cdp)
        self.assertEqual(l3ca["cdp_on"], mock_l3ca.cdp_on)

    def test_cli_get_capability_mon(self, mock_load_lib, mock_find_lib):
        """
        Test checking monitoring capability pqos using cli
        """
        mock_cap = PqosCapMockBuilder()
        mock_cap.append_to_capability_array("l3ca")
        mock_cap.append_to_capability_array("mon")
        p_cap = mock_cap.build_cap()

        mock_cpu = PqosCpuMockBuilder()
        p_cpu = mock_cpu.build_cpu(8)

        def pqos_cap_get_mock(cap, cpu):
            """
            mock pqos_cap_get function
            """
            if cpu:
                ctypes.memmove(cpu, ctypes.addressof(p_cpu), ctypes.sizeof(p_cpu))
            if cap:
                ctypes.memmove(cap, ctypes.addressof(p_cap), ctypes.sizeof(p_cap))
            return Log.PQOS_RETVAL_OK

        wrapper = PqosWrapper(["pqos_wrapper", "-c", "mon"])
        wrapper.lib.pqos_cap_get = MagicMock(side_effect=pqos_cap_get_mock)
        wrapper.execute_cli()
        self.result_string.seek(0)
        result_json = json.load(self.result_string)
        self.assertEqual(
            result_json["check_capability"]["function"], "get_capability_info"
        )
        self.assertEqual(result_json["check_capability"]["error"], False)
        self.assertEqual(
            result_json["check_capability"]["returncode"], Log.PQOS_RETVAL_OK
        )
        mon = result_json["check_capability"]["function_output"]
        mock_mon = mock_cap.build_monitoring_capability()
        self.assertEqual(mon["mem_size"], mock_mon.mem_size)
        self.assertEqual(mon["max_rmid"], mock_mon.max_rmid)
        self.assertEqual(mon["l3_size"], mock_mon.l3_size)
        self.assertEqual(mon["num_events"], mock_mon.num_events)
        self.assertEqual(len(mon["events"]), mock_mon.num_events)

    def test_cli_allocate_resource(self, mock_load_lib, mock_find_lib):
        """
        Test checking capability pqos using cli
        """
        mock_cap = PqosCapMockBuilder()
        mock_cap.append_to_capability_array("l3ca")
        mock_cap.append_to_capability_array("l2ca")
        p_cap = mock_cap.build_cap()

        mock_cpu = PqosCpuMockBuilder()
        p_cpu = mock_cpu.build_cpu(8)

        def pqos_cap_get_mock(cap, cpu):
            """
            mock pqos_cap_get function
            """
            if cpu:
                ctypes.memmove(cpu, ctypes.addressof(p_cpu), ctypes.sizeof(p_cpu))
            if cap:
                ctypes.memmove(cap, ctypes.addressof(p_cap), ctypes.sizeof(p_cap))
            return Log.PQOS_RETVAL_OK

        wrapper = PqosWrapper(["pqos_wrapper", "-a", "l3ca", "[[0,1], [2,3]]"])
        wrapper.lib.pqos_cap_get = MagicMock(side_effect=pqos_cap_get_mock)
        wrapper.execute_cli()
        self.result_string.seek(0)
        result_json = json.load(self.result_string)
        self.assertEqual(
            result_json["allocate_resource"]["function"], "allocate_resource"
        )
        self.assertEqual(result_json["allocate_resource"]["error"], False)
        self.assertEqual(
            result_json["allocate_resource"]["returncode"], Log.PQOS_RETVAL_OK
        )
        output = result_json["allocate_resource"]["function_output"]
        self.assertEqual(output["cores"]["0"], 0)
        self.assertEqual(output["cores"]["1"], 0)
        self.assertEqual(output["cores"]["2"], 1)
        self.assertEqual(output["cores"]["3"], 1)

        self.assertEqual(output["sockets"]["0"]["0"], int("00001111", 2))
        self.assertEqual(output["sockets"]["0"]["1"], int("11110000", 2))

    def test_cli_show_resource(self, mock_load_lib, mock_find_lib):
        """
        Test checking capability pqos using cli
        """
        mock_cap = PqosCapMockBuilder()
        mock_cap.append_to_capability_array("l3ca")
        mock_cap.append_to_capability_array("l2ca")
        p_cap = mock_cap.build_cap()

        mock_cpu = PqosCpuMockBuilder()
        p_cpu = mock_cpu.build_cpu(8)

        def pqos_cap_get_mock(cap, cpu):
            """
            mock pqos_cap_get function
            """
            if cpu:
                ctypes.memmove(cpu, ctypes.addressof(p_cpu), ctypes.sizeof(p_cpu))
            if cap:
                ctypes.memmove(cap, ctypes.addressof(p_cap), ctypes.sizeof(p_cap))
            return Log.PQOS_RETVAL_OK

        wrapper = PqosWrapper(["pqos_wrapper", "-s"])
        wrapper.lib.pqos_cap_get = MagicMock(side_effect=pqos_cap_get_mock)
        wrapper.execute_cli()
        self.result_string.seek(0)
        result_json = json.load(self.result_string)
        self.assertEqual(
            result_json["show_association"]["function"], "show_association"
        )
        self.assertEqual(result_json["show_association"]["error"], False)
        self.assertEqual(
            result_json["show_association"]["returncode"], Log.PQOS_RETVAL_OK
        )
        output = result_json["show_association"]["function_output"]
        for core in range(8):
            self.assertEqual(output["cores"][str(core)], 0)

        for socket in range(2):
            for cos in range(2):
                self.assertEqual(output["sockets"][str(socket)][str(cos)], 65535)
