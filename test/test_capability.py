# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    Unit tests for capability module
"""

import unittest
import json
from unittest.mock import MagicMock
from pqos_wrapper.capability import PqosCapability
from pqos_wrapper.utils import Log
from test.mock_pqos_cap import (
    PqosCapMockBuilder,
    pqos_cap_get_type_mock,
    prepare_cap_lib_false,
    pqos_l3ca_get_cos_num_mock,
)


class TestCapability(unittest.TestCase):
    """
    Testing class for PqosCapability functions
    """

    def test_cap_init(self):
        """
        Test PqosCapability initialisation
        """
        mock_cap = PqosCapMockBuilder()
        mock_pqos = mock_cap.build_mock(["l3ca"])
        cap = PqosCapability(mock_pqos)
        self.assertIsInstance(cap, PqosCapability)

    def test_cap_init_error(self):
        """
        Check if PqosCapability raises correct error on failing initialization.
        """
        mock_pqos = prepare_cap_lib_false()
        with self.assertRaises(SystemExit) as err:
            PqosCapability(mock_pqos)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_cap_get")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)

    def test_cap_get_type_l3ca(self):
        """
        Test succcessful extraction of l3ca capability.
        """
        mock_cap = PqosCapMockBuilder()
        mock_pqos = mock_cap.build_mock(["l3ca"])
        mock_pqos.pqos_cap_get_type = MagicMock(side_effect=pqos_cap_get_type_mock)
        cap = PqosCapability(mock_pqos)
        l3ca = cap.get_type("l3ca")["function_output"]
        mock_l3ca = PqosCapMockBuilder.build_l3ca_capability()
        self.assertEqual(l3ca["mem_size"], mock_l3ca.mem_size)
        self.assertEqual(l3ca["num_classes"], mock_l3ca.num_classes)
        self.assertEqual(l3ca["num_ways"], mock_l3ca.num_ways)
        self.assertEqual(l3ca["way_size"], mock_l3ca.way_size)
        self.assertEqual(l3ca["way_contention"], mock_l3ca.way_contention)
        self.assertEqual(l3ca["cdp"], mock_l3ca.cdp)
        self.assertEqual(l3ca["cdp_on"], mock_l3ca.cdp_on)

    def test_cap_get_type_l2ca(self):
        """
        Test succcessful extraction of l2ca capability.
        """
        mock_cap = PqosCapMockBuilder()
        mock_pqos = mock_cap.build_mock(["l3ca", "l2ca"])
        mock_pqos.pqos_cap_get_type = MagicMock(side_effect=pqos_cap_get_type_mock)
        cap = PqosCapability(mock_pqos)
        l2ca = cap.get_type("l2ca")["function_output"]
        mock_l2ca = PqosCapMockBuilder.build_l2ca_capability()
        self.assertEqual(l2ca["mem_size"], mock_l2ca.mem_size)
        self.assertEqual(l2ca["num_classes"], mock_l2ca.num_classes)
        self.assertEqual(l2ca["num_ways"], mock_l2ca.num_ways)
        self.assertEqual(l2ca["way_size"], mock_l2ca.way_size)
        self.assertEqual(l2ca["way_contention"], mock_l2ca.way_contention)
        self.assertEqual(l2ca["cdp"], mock_l2ca.cdp)
        self.assertEqual(l2ca["cdp_on"], mock_l2ca.cdp_on)

    def test_cap_get_type_l3ca_error(self):
        """
        Check if pqos-wrapper raises correct error on failing extraction of l3ca capability.
        """
        mock_cap = PqosCapMockBuilder()
        mock_pqos = mock_cap.build_mock(["l2ca"])
        mock_pqos.pqos_cap_get_type = MagicMock(side_effect=pqos_cap_get_type_mock)
        cap = PqosCapability(mock_pqos)
        with self.assertRaises(SystemExit) as err:
            cap.get_type("l3ca")

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_cap_get_type")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_RESOURCE)
        self.assertEqual(error_json["error"], True)

    def test_cap_get_type_l2ca_error(self):
        """
        Check if pqos-wrapper raises correct error on failing extraction of l3ca capability.
        """
        mock_cap = PqosCapMockBuilder()
        mock_pqos = mock_cap.build_mock(["l3ca"])
        mock_pqos.pqos_cap_get_type = MagicMock(side_effect=pqos_cap_get_type_mock)
        cap = PqosCapability(mock_pqos)
        with self.assertRaises(SystemExit) as err:
            cap.get_type("l2ca")

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_cap_get_type")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_RESOURCE)
        self.assertEqual(error_json["error"], True)

    def test_cap_get_type_mon(self):
        """
        Test successful extraction of monitoring capability.
        """
        mock_cap = PqosCapMockBuilder()
        mock_pqos = mock_cap.build_mock(["l3ca", "l2ca", "mon"])
        mock_pqos.pqos_cap_get_type = MagicMock(side_effect=pqos_cap_get_type_mock)
        cap = PqosCapability(mock_pqos)
        mon = cap.get_type("mon")["function_output"]
        mock_mon = mock_cap.build_monitoring_capability()
        self.assertEqual(mon["mem_size"], mock_mon.mem_size)
        self.assertEqual(mon["max_rmid"], mock_mon.max_rmid)
        self.assertEqual(mon["l3_size"], mock_mon.l3_size)
        self.assertEqual(mon["num_events"], mock_mon.num_events)
        self.assertEqual(len(mon["events"]), mock_mon.num_events)

    def test_cap_get_type_mon_error(self):
        """
        Check if pqos-wrapper raises correct error on failing extraction of monitoring capability.
        """
        mock_cap = PqosCapMockBuilder()
        mock_pqos = mock_cap.build_mock(["l3ca", "l2ca"])
        mock_pqos.pqos_cap_get_type = MagicMock(side_effect=pqos_cap_get_type_mock)
        cap = PqosCapability(mock_pqos)
        with self.assertRaises(SystemExit) as err:
            cap.get_type("mon")

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_cap_get_type")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_RESOURCE)
        self.assertEqual(error_json["error"], True)

    def test_cap_get_l3ca_cos_num(self):
        """
        Test get_l3ca_cos_num function
        """
        mock_cap = PqosCapMockBuilder()
        mock_pqos = mock_cap.build_mock(["l3ca"])
        mock_pqos.pqos_l3ca_get_cos_num = MagicMock(
            side_effect=pqos_l3ca_get_cos_num_mock
        )
        cap = PqosCapability(mock_pqos)
        num_classes = cap.get_l3ca_cos_num()
        mock_l3ca = mock_cap.build_l3ca_capability()
        self.assertEqual(num_classes, mock_l3ca.num_classes)

    def test_cap_get_l3ca_cos_num_error(self):
        """
        Check if get_l3ca_cos_num function raises correct error
        when l3ca capability is not initialised
        """
        mock_cap = PqosCapMockBuilder()
        mock_pqos = mock_cap.build_mock(["l2ca"])
        mock_pqos.pqos_l3ca_get_cos_num = MagicMock(
            side_effect=pqos_l3ca_get_cos_num_mock
        )
        cap = PqosCapability(mock_pqos)
        with self.assertRaises(SystemExit) as err:
            cap.get_l3ca_cos_num()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_l3ca_get_cos_num")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_RESOURCE)
        self.assertEqual(error_json["error"], True)
