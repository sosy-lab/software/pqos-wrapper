# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    Unit tests for utils module
"""
import json
import unittest
from pqos_wrapper.utils import get_core_list, Log


class TestUtils(unittest.TestCase):
    """
    Testing class for utils module
    """

    def test_utils_get_core_list(self):
        core_string = "[[0,1],[2,3]]"
        core_list = get_core_list(core_string)
        self.assertEqual(core_list, [[0, 1], [2, 3]])

    def test_utils_get_core_list_single_list_error(self):
        core_string = "[0,1]"
        with self.assertRaises(SystemExit) as err:
            get_core_list(core_string)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "get_core_list")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_utils_get_core_list_empty_list_error(self):
        core_string = "[[],[0,1]]"
        with self.assertRaises(SystemExit) as err:
            get_core_list(core_string)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "get_core_list")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_utils_get_core_list_invalid_string_error(self):
        core_string = ["a", "b", "c"]
        with self.assertRaises(SystemExit) as err:
            get_core_list(core_string)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "get_core_list")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_utils_get_core_list_invalid_list_error(self):
        core_string = "[0, 1, 2]]"
        with self.assertRaises(SystemExit) as err:
            get_core_list(core_string)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "get_core_list")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_utils_get_core_list_with_dict_error(self):
        core_string = "{'a' : '1', 'b': '2'}"
        with self.assertRaises(SystemExit) as err:
            get_core_list(core_string)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "get_core_list")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)
