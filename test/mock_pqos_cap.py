# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    This module defines mock pqos library functions and mock CPqosCap ctypes structures for
    testing capability module
"""
import ctypes
from unittest.mock import Mock, MagicMock
from pqos_wrapper.capability import (
    CPqosCapabilityL3,
    CPqosCapabilityL2,
    CPqosCap,
    CPqosCapabilityMonitoring,
    CPqosMonitor,
    CPqosCapabilityUnion,
    CPqosCapability,
)
from pqos_wrapper.utils import Log
from test.helper import ctypes_build_array, ctypes_ref_set_int


class PqosCapMockBuilder(object):
    """
    Builds a mock CPqosCap object.
    """

    PQOS_CAP_TYPE_MON = 0
    PQOS_CAP_TYPE_L3CA = 1
    PQOS_CAP_TYPE_L2CA = 2

    def __init__(self):
        self.capabilities = []
        self.events = (CPqosMonitor * 3)()

    @staticmethod
    def build_l3ca_capability():
        """
        Builds mock CPqosCapabilityL3 object. Might be overwritten
        in a subclass if neccessary.
        """
        l3ca = CPqosCapabilityL3(
            mem_size=ctypes.sizeof(CPqosCapabilityL3),
            num_classes=2,
            num_ways=8,
            way_size=1024 * 1024,
            way_contention=0,
            cdp=1,
            cdp_on=0,
        )
        return l3ca

    @staticmethod
    def build_l2ca_capability():
        """
        Builds mock CPqosCapabilityL2 object. Might be overwritten
        in a subclass if neccessary.
        """
        l2ca = CPqosCapabilityL2(
            mem_size=ctypes.sizeof(CPqosCapabilityL2),
            num_classes=2,
            num_ways=8,
            way_size=1024 * 1024,
            way_contention=0,
            cdp=1,
            cdp_on=0,
        )
        return l2ca

    def build_monitoring_capability(self):
        """
        Builds mock CPqosCapabilityMonitoring object. Might be overwritten
        in a subclass if neccessary.
        """
        self.build_events()
        events = (CPqosMonitor * 0).from_address(ctypes.addressof(self.events))
        mon = CPqosCapabilityMonitoring(
            mem_size=ctypes.sizeof(CPqosCapabilityMonitoring),
            max_rmid=0,
            l3_size=0,
            num_events=3,
            events=events,
        )
        return mon

    def build_events(self):
        """
        Builds an array of CPqosMonitor objects and stores it in events instance variable.
        """
        event = CPqosMonitor(type=1, max_rmid=0, scale_factor=0)
        self.events = ctypes_build_array([event, event, event])

    def append_to_capability_array(self, __type):
        """
        Appends requested capabilites object to capabilities list.
        """
        if __type == "l3ca":
            l3ca = self.build_l3ca_capability()
            l3ca_u = CPqosCapabilityUnion(l3ca=ctypes.pointer(l3ca))
            l3ca_cap = CPqosCapability(type=self.PQOS_CAP_TYPE_L3CA, u=l3ca_u)
            self.capabilities.append(l3ca_cap)

        elif __type == "l2ca":
            l2ca = self.build_l2ca_capability()
            l2ca_u = CPqosCapabilityUnion(l2ca=ctypes.pointer(l2ca))
            l2ca_cap = CPqosCapability(type=self.PQOS_CAP_TYPE_L2CA, u=l2ca_u)
            self.capabilities.append(l2ca_cap)

        elif __type == "mon":
            mon = self.build_monitoring_capability()
            mon_u = CPqosCapabilityUnion(mon=ctypes.pointer(mon))
            mon_cap = CPqosCapability(type=self.PQOS_CAP_TYPE_MON, u=mon_u)
            self.capabilities.append(mon_cap)

    def build_cap(self):
        """
        Builds mock CPqosCap object.
        """
        num_cap = len(self.capabilities)
        cap_mem_size = ctypes.sizeof(CPqosCap) + (
            num_cap * ctypes.sizeof(CPqosCapability)
        )
        cap = CPqosCap(mem_size=cap_mem_size, version=123, num_cap=num_cap)
        buf = (ctypes.c_char * cap_mem_size)()
        cap_arr = ctypes_build_array(self.capabilities)
        cap_size = ctypes.sizeof(cap)
        ctypes.memmove(buf, ctypes.addressof(cap), cap_size)
        ctypes.memmove(
            ctypes.byref(buf, cap_size),
            ctypes.addressof(cap_arr),
            ctypes.sizeof(cap_arr),
        )
        return ctypes.cast(ctypes.pointer(buf), ctypes.POINTER(type(cap)))

    def build_mock(self, capabilities):
        """
        Build mock PqosCap object with given capabilities and return a mock_pqos lib
        with pqos_cap_get function
        """
        for capability in capabilities:
            self.append_to_capability_array(capability)
        self.p_cap = self.build_cap()
        mock_pqos = Mock()

        def pqos_cap_get_mock(cap, cpu):
            """
            mock pqos_cap_get function
            """
            ctypes.memmove(cap, ctypes.addressof(self.p_cap), ctypes.sizeof(self.p_cap))
            return Log.PQOS_RETVAL_OK

        mock_pqos.pqos_cap_get = MagicMock(side_effect=pqos_cap_get_mock)
        return mock_pqos


def prepare_cap_lib_false():
    """
    Prepare a false mock pqos library for testing error handling of PqosCapability
    """
    mock_pqos = Mock()
    mock_pqos.pqos_cap_get = MagicMock(return_value=Log.PQOS_RETVAL_ERROR)
    return mock_pqos


def pqos_cap_get_type_mock(p_cap, type_enum, p_cap_item_ref):
    """
    mock pqos_cap_get_type function
    """
    cap_arr_type = CPqosCapability * p_cap.contents.num_cap
    cap_arr = cap_arr_type.from_address(ctypes.addressof(p_cap.contents.capabilities))
    for cap in cap_arr:
        if type_enum == cap.type:
            cap_ptr = ctypes.pointer(cap)
            ctypes.memmove(
                p_cap_item_ref, ctypes.addressof(cap_ptr), ctypes.sizeof(type(cap_ptr))
            )
            return Log.PQOS_RETVAL_OK
    return Log.PQOS_RETVAL_RESOURCE


def get_type_mock(__type):
    """
    mock get_type function of capability module
    """
    ret = {
        "message": "success",
        "returncode": Log.PQOS_RETVAL_OK,
        "function_output": {},
    }
    l3ca = PqosCapMockBuilder.build_l3ca_capability()
    for field in l3ca._fields_:
        ret["function_output"][field[0]] = getattr(l3ca, field[0])
    return ret


def pqos_l3ca_get_cos_num_mock(p_cap, cos_num_ref):
    """
    mock pqos_l3ca_get_cos_num function
    """
    cap_arr_type = CPqosCapability * p_cap.contents.num_cap
    cap_arr = cap_arr_type.from_address(ctypes.addressof(p_cap.contents.capabilities))
    for cap in cap_arr:
        if cap.type == PqosCapMockBuilder.PQOS_CAP_TYPE_L3CA:
            cos_num = cap.u.l3ca.contents.num_classes
            ctypes_ref_set_int(cos_num_ref, cos_num)
            return Log.PQOS_RETVAL_OK
    return Log.PQOS_RETVAL_RESOURCE
