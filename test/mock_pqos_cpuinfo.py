# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    This module defines mock pqos library functions and mock CPqosCpuInfo ctypes structures for
    testing capability module
"""
import ctypes
from unittest.mock import Mock, MagicMock
from pqos_wrapper.cpuinfo import CPqosCacheInfo, CPqosCoreInfo, CPqosCpuInfo
from pqos_wrapper.utils import Log
from test.helper import ctypes_build_array, ctypes_ref_set_int


class PqosCpuMockBuilder:
    """
    Class to build mock CPqosCpuInfo ctypes object
    """

    def __init__(self):
        self.p_cpu = None

    @staticmethod
    def build_cache_info():
        """
        Build mock cache information
        """
        return CPqosCacheInfo(
            detected=1,
            num_ways=2,
            num_sets=1,
            num_partitions=1,
            line_size=64 * 1024,
            total_size=2 * 1024 * 1024,
            way_size=1024 * 1024,
        )

    @staticmethod
    def build_core_info(core):
        """
        Build mock cpu core info
        """
        return CPqosCoreInfo(lcore=core, socket=int(core // 4), l3_id=0, l2_id=0)

    def build_cpu(self, num_cores):
        """
        Builds mock CPqosCpuInfo object
        """
        l2_info = self.build_cache_info()
        l3_info = self.build_cache_info()
        cores_info = []
        for core in range(num_cores):
            cores_info.append(self.build_core_info(core))
        cpuinfo_mem_size = ctypes.sizeof(CPqosCpuInfo) + (
            num_cores * ctypes.sizeof(CPqosCoreInfo)
        )
        buf = (ctypes.c_char * cpuinfo_mem_size)()

        cpuinfo = CPqosCpuInfo(
            mem_size=cpuinfo_mem_size, l2=l2_info, l3=l3_info, num_cores=num_cores
        )

        cpu_size = ctypes.sizeof(cpuinfo)
        cpuinfo_array = ctypes_build_array(cores_info)
        ctypes.memmove(buf, ctypes.addressof(cpuinfo), cpu_size)
        ctypes.memmove(
            ctypes.byref(buf, cpu_size),
            ctypes.addressof(cpuinfo_array),
            ctypes.sizeof(cpuinfo_array),
        )
        return ctypes.cast(ctypes.pointer(buf), ctypes.POINTER(type(cpuinfo)))

    def build_mock(self, cores):
        """
        Builds mock pqos lib with pqos_cap_get
        """
        self.p_cpu = self.build_cpu(cores)
        mock_pqos = Mock()

        def pqos_cap_get_mock(cap, cpu):
            """
            mock pqos_cap_get function
            """
            ctypes.memmove(cpu, ctypes.addressof(self.p_cpu), ctypes.sizeof(self.p_cpu))
            return Log.PQOS_RETVAL_OK

        mock_pqos.pqos_cap_get = MagicMock(side_effect=pqos_cap_get_mock)
        return mock_pqos


def pqos_cpu_get_sockets_mock(p_cpu, count_ref):
    """
    mock pqos_cpu_get_sockets function
    """
    socket_set = set()
    num_cores = p_cpu.contents.num_cores
    core_array = (CPqosCoreInfo * num_cores).from_address(
        ctypes.addressof(p_cpu.contents.cores)
    )
    for core in core_array:
        socket_set.add(core.socket)
    ctypes_ref_set_int(count_ref, len(socket_set))
    sockets_arr = ctypes_build_array([ctypes.c_uint(socket) for socket in socket_set])
    return ctypes.cast(sockets_arr, ctypes.POINTER(ctypes.c_uint))


def pqos_cpu_get_core_info_mock(p_cpu, core_id):
    """
    mock pqos_cpu_get_core_info function
    """
    num_cores = p_cpu.contents.num_cores
    core_array = (CPqosCoreInfo * num_cores).from_address(
        ctypes.addressof(p_cpu.contents.cores)
    )
    ret = None
    for core in core_array:
        if core.lcore == core_id:
            ret = ctypes.pointer(core)
            break
    return ret
