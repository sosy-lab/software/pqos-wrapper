# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    Unit tests for l3ca module
"""
import unittest
import json
from unittest.mock import MagicMock
from pqos_wrapper.l3ca import PqosCatL3, L3COS
from pqos_wrapper.utils import Log
from test.mock_pqos_l3ca import (
    build_mock_cap_and_pqos_lib,
    pqos_l3ca_get_mock,
    prepare_l3ca_set,
    pqos_l3ca_get_min_cbm_bits_mock,
)


class TestL3Ca(unittest.TestCase):
    """
    Testing class for l3ca module
    """

    @classmethod
    def setUpClass(cls):
        cls.num_classes = 4
        cls.mock_cap, cls.mock_pqos = build_mock_cap_and_pqos_lib(cls.num_classes)

    def test_l3ca_init(self):
        """
        Test initialisation of PqosCatL3 class
        """
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        self.assertIsInstance(l3ca, PqosCatL3)

    def test_l3ca_get(self):
        """
        Test get function to read class of service from a socket
        """
        self.mock_pqos.pqos_l3ca_get = MagicMock(side_effect=pqos_l3ca_get_mock)
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        coses = l3ca.get(0)
        self.assertEqual(len(coses), self.num_classes)

        for cos in range(self.num_classes):
            self.assertEqual(coses[cos].class_id, cos)
            self.assertEqual(coses[cos].mask, 0xFFFF)

    def test_l3ca_get_error(self):
        """
        Check if get function raises correct error on failing.
        """
        self.mock_pqos.pqos_l3ca_get = MagicMock(return_value=Log.PQOS_RETVAL_ERROR)
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        with self.assertRaises(SystemExit) as err:
            l3ca.get(0)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_l3ca_get")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)

    def test_l3ca_set(self):
        """
        Test l3ca set function
        """
        pqos_l3ca_set_mock = prepare_l3ca_set(self.num_classes, 16, 2)
        self.mock_pqos.pqos_l3ca_set = MagicMock(side_effect=pqos_l3ca_set_mock)
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        l3ca.set(0, [L3COS(0, 0x000F)])
        self.mock_pqos.pqos_l3ca_set.assert_called_once_with(0, 1, unittest.mock.ANY)

    def test_l3ca_set_multiple(self):
        """
        Test l3ca set function
        """
        pqos_l3ca_set_mock = prepare_l3ca_set(self.num_classes, 16, 2)
        self.mock_pqos.pqos_l3ca_set = MagicMock(side_effect=pqos_l3ca_set_mock)
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        l3ca.set(0, [L3COS(0, 0x000F), L3COS(1, 0x000F), L3COS(2, 0x000F)])
        self.mock_pqos.pqos_l3ca_set.assert_called_once_with(0, 3, unittest.mock.ANY)

    def test_l3ca_set_error_incorrect_class_id(self):
        """
        Check if l3ca set function raises error if incorrect class id
        is provided
        """
        pqos_l3ca_set_mock = prepare_l3ca_set(self.num_classes, 16, 2)
        self.mock_pqos.pqos_l3ca_set = MagicMock(side_effect=pqos_l3ca_set_mock)
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        with self.assertRaises(SystemExit) as err:
            l3ca.set(0, [L3COS(4, 0x000F)])

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_l3ca_set")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_l3ca_set_error_non_contiguous_bits(self):
        """
        Check if l3ca set function raises error if non contiguous bits are set
        """
        pqos_l3ca_set_mock = prepare_l3ca_set(self.num_classes, 16, 2)
        self.mock_pqos.pqos_l3ca_set = MagicMock(side_effect=pqos_l3ca_set_mock)
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        with self.assertRaises(SystemExit) as err:
            l3ca.set(0, [L3COS(0, 0x000D)])

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_l3ca_set")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_l3ca_set_error_less_than_min_bits(self):
        """
        Check if l3ca set function raises error if less than min cbm bits are set
        """
        pqos_l3ca_set_mock = prepare_l3ca_set(self.num_classes, 16, 5)
        self.mock_pqos.pqos_l3ca_set = MagicMock(side_effect=pqos_l3ca_set_mock)
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        with self.assertRaises(SystemExit) as err:
            l3ca.set(0, [L3COS(0, 0x000F)])

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_l3ca_set")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_l3ca_set_error_more_than_max_bits(self):
        """
        Check if l3ca set function raises error if less than min cbm bits are set
        """
        pqos_l3ca_set_mock = prepare_l3ca_set(self.num_classes, 10, 5)
        self.mock_pqos.pqos_l3ca_set = MagicMock(side_effect=pqos_l3ca_set_mock)
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        with self.assertRaises(SystemExit) as err:
            l3ca.set(0, [L3COS(0, 0xFFFF)])

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_l3ca_set")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_l3ca_get_min_cbm_bits(self):
        """
        Test get_min_cbm_bits function of l3ca module.
        """
        self.mock_pqos.pqos_l3ca_get_min_cbm_bits = MagicMock(
            side_effect=pqos_l3ca_get_min_cbm_bits_mock
        )
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        min_bits = l3ca.get_min_cbm_bits()
        self.assertEqual(min_bits, 2)

    def test_l3ca_get_min_cbm_bits_error(self):
        """
        Check if get_min_bits function raises correct error on failing
        """
        self.mock_pqos.pqos_l3ca_get_min_cbm_bits = MagicMock(
            return_value=Log.PQOS_RETVAL_ERROR
        )
        l3ca = PqosCatL3(self.mock_pqos, self.mock_cap)
        with self.assertRaises(SystemExit) as err:
            l3ca.get_min_cbm_bits()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_l3ca_get_min_cbm_bits")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)
