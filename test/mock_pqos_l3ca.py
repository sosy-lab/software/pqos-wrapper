# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    This module defines mock pqos library functions and mock CPqosL3Ca ctypes structures for
    testing l3ca module
"""

import ctypes
from unittest.mock import Mock, MagicMock
from pqos_wrapper.l3ca import CPqosL3Ca
from pqos_wrapper.utils import Log
from test.helper import ctypes_build_array, ctypes_ref_set_int


def build_mock_cap_and_pqos_lib(num_classes):
    """
    Returns a mock PqosCapability object and a mock pqos library with functions required for l3ca.
    """
    mock_cap = Mock()
    mock_cap.get_l3ca_cos_num = MagicMock(return_value=num_classes)
    mock_pqos = Mock()
    return mock_cap, mock_pqos


def pqos_l3ca_get_mock(socket, cos_num, num_ca_ref, l3cas):
    """
    mock pqos_l3ca_get function
    """
    cos_arr = []
    for cos in range(cos_num):
        cos_c = CPqosL3Ca(class_id=cos, cdp=0, ways_mask=0xFFFF)
        cos_arr.append(cos_c)
    cos_arr_c = ctypes_build_array(cos_arr)
    ctypes.memmove(l3cas, cos_arr_c, ctypes.sizeof(cos_arr_c))
    ctypes_ref_set_int(num_ca_ref, len(cos_arr_c))
    return Log.PQOS_RETVAL_OK


def prepare_l3ca_set(num_classes, max_bits, min_bits):
    """
    prepare function for mocking pqos_l3ca_set function
    """

    def pqos_l3ca_set_mock(socket, cos_num, l3_ca_arr):
        """
        mock pqos_l3ca_set function
        """
        for i in range(cos_num):
            if (
                l3_ca_arr[i].class_id < 0
                or l3_ca_arr[i].class_id >= num_classes
                or not valid_mask(l3_ca_arr[i].u.ways_mask, max_bits, min_bits)
            ):
                return Log.PQOS_RETVAL_PARAM
        return Log.PQOS_RETVAL_OK

    return pqos_l3ca_set_mock


def valid_mask(mask_int, max_bits, min_bits):
    """
    Check if l3ca ways mask is contiguous and the no of bits
    are within the boundary limits.
    """
    binary_string = "{0:b}".format(mask_int)
    if len(binary_string) > max_bits or len(binary_string) < min_bits:
        return False
    for bit in range(1, len(binary_string) - 1):
        if (
            binary_string[bit] == "0"
            and binary_string[bit - 1] == "1"
            and binary_string[bit + 1] == "1"
        ):
            return False
    return True


def pqos_l3ca_get_min_cbm_bits_mock(min_cbm_bits_ref):
    """
    mock pqos_l3ca_get_min_cbm_bits function
    """
    ctypes_ref_set_int(min_cbm_bits_ref, 2)
    return Log.PQOS_RETVAL_OK
