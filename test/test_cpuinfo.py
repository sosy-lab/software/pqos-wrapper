# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    Unit tests for cpuinfo module
"""
import json
import unittest
from unittest.mock import MagicMock
from pqos_wrapper.cpuinfo import PqosCpuInfo
from pqos_wrapper.utils import Log
from test.mock_pqos_cap import prepare_cap_lib_false
from test.mock_pqos_cpuinfo import (
    PqosCpuMockBuilder,
    pqos_cpu_get_sockets_mock,
    pqos_cpu_get_core_info_mock,
)


class TestCpuInfo(unittest.TestCase):
    """
    Testing class for PqosCpuInfo functions
    """

    def test_cpu_init(self):
        """
        Test PqosCpuInfo initialisation
        """
        mock_cpu = PqosCpuMockBuilder()
        mock_pqos = mock_cpu.build_mock(8)
        cpu_info = PqosCpuInfo(mock_pqos)
        self.assertIsInstance(cpu_info, PqosCpuInfo)

    def test_cpu_init_error(self):
        """
        Check if PqosCpuInfo raises correct error on failing initialization.
        """
        mock_pqos = prepare_cap_lib_false()
        with self.assertRaises(SystemExit) as err:
            PqosCpuInfo(mock_pqos)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_cap_get")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)

    def test_cpu_get_sockets(self):
        """
        Test get_sockets function
        """
        num_cores = 8
        mock_cpu = PqosCpuMockBuilder()
        mock_pqos = mock_cpu.build_mock(num_cores)
        mock_pqos.pqos_cpu_get_sockets = MagicMock(
            side_effect=pqos_cpu_get_sockets_mock
        )
        cpu_info = PqosCpuInfo(mock_pqos)
        sockets = cpu_info.get_sockets()
        self.assertEqual(len(sockets), int(num_cores // 4))
        for socket_num in range(len(sockets)):
            self.assertEqual(sockets[socket_num], socket_num)

    def test_cpu_get_core_info(self):
        """
        Test get_core_info function
        """
        mock_cpu = PqosCpuMockBuilder()
        mock_pqos = mock_cpu.build_mock(8)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        cpu_info = PqosCpuInfo(mock_pqos)
        core_info = cpu_info.get_core_info(0)
        mock_core = mock_cpu.build_core_info(0)
        self.assertEqual(core_info.core, mock_core.lcore)
        self.assertEqual(core_info.socket, mock_core.socket)
        self.assertEqual(core_info.l2_id, mock_core.l2_id)
        self.assertEqual(core_info.l3_id, mock_core.l3_id)

    def test_cpu_get_core_info_error(self):
        """
        Check if pqos-wrapper raises correct error on failing get_core_info.
        """
        mock_cpu = PqosCpuMockBuilder()
        mock_pqos = mock_cpu.build_mock(8)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        cpu_info = PqosCpuInfo(mock_pqos)
        with self.assertRaises(SystemExit) as err:
            cpu_info.get_core_info(10)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_cpu_get_core_info")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)
