# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    Unit tests for monitor module
"""
import copy
import json
import unittest
from unittest.mock import MagicMock
from pqos_wrapper.capability import PqosCapability
from pqos_wrapper.monitor import PqosMonitor, CPqosMonData
from pqos_wrapper.utils import Log
from test.mock_pqos_cpuinfo import PqosCpuMockBuilder
from test.mock_pqos_cap import PqosCapMockBuilder, pqos_cap_get_type_mock


class TestMonitor(unittest.TestCase):
    """
    Unit tests class for monitoring module
    """

    def setUp(self):
        mock_cpu = PqosCpuMockBuilder()
        self.mock_pqos = mock_cpu.build_mock(8)
        mock_cap = PqosCapMockBuilder()
        mock_pqos_cap = mock_cap.build_mock(["mon"])
        mock_pqos_cap.pqos_cap_get_type = MagicMock(side_effect=pqos_cap_get_type_mock)
        cap = PqosCapability(mock_pqos_cap)
        self.mon_cap = cap.get_type("mon")["function_output"]
        self.mon_cap["events"][0]["type"] = 1
        self.mon_cap["events"][1]["type"] = 4
        self.mon_cap["events"][2]["type"] = 16384

    def test_mon_init(self):
        """
        Test initialisation of PqosMonitor class
        """
        mon = PqosMonitor(self.mock_pqos, False)
        self.assertIsInstance(mon, PqosMonitor)

    def test_mon_get_available_events(self):
        """
        Test get_available_events function
        """
        mon = PqosMonitor(self.mock_pqos, False)
        all_evts = mon.get_available_events(self.mon_cap)
        monitored_events = ["cores", "llc_misses", "llc", "mbm_total"]
        non_monitored_events = ["ipc", "mbm_local", "mbm_remote"]
        for event in monitored_events:
            self.assertIn(event, mon.available_mon_events.keys())
        for event in non_monitored_events:
            self.assertNotIn(event, mon.available_mon_events.keys())
        self.assertEqual(mon.header_attr, monitored_events)
        self.assertEqual(all_evts, (1 | 4 | 16384))

    def test_mon_setup_monitoring(self):
        """
        Test setup_monitoring function
        """
        self.mock_pqos.pqos_mon_start = MagicMock(return_value=Log.PQOS_RETVAL_OK)
        mon = PqosMonitor(self.mock_pqos, False)
        mon.core_list = [[0, 1], [2, 3]]
        mon.mon_data = (CPqosMonData * len(mon.core_list))()
        all_evts = mon.get_available_events(self.mon_cap)
        for _core_set in mon.core_list:
            mon.mon_data_avg.append(mon.available_mon_events.copy())
        mon.setup_monitoring(all_evts)

    def test_mon_setup_monitoring_error(self):
        """
        Test setup_monitoring function when pqos_mon_start function fails
        """
        self.mock_pqos.pqos_mon_start = MagicMock(return_value=Log.PQOS_RETVAL_ERROR)
        mon = PqosMonitor(self.mock_pqos, False)
        mon.core_list = [[0, 1], [2, 3]]
        mon.mon_data = (CPqosMonData * len(mon.core_list))()
        all_evts = mon.get_available_events(self.mon_cap)
        for _core_set in mon.core_list:
            mon.mon_data_avg.append(mon.available_mon_events.copy())

        with self.assertRaises(SystemExit) as err:
            mon.setup_monitoring(all_evts)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_mon_start")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)

    def test_mon_store_monitoring_info(self):
        """
        Test get_available_events function
        """
        mon = PqosMonitor(self.mock_pqos, False)
        mon.core_list = [[0, 1], [2, 3]]
        mon.mon_data = (CPqosMonData * len(mon.core_list))()
        mon.get_available_events(self.mon_cap)
        for _core_set in mon.core_list:
            mon.mon_data_avg.append(copy.deepcopy(mon.available_mon_events))
        mon.mon_data_avg[0]["cores"] = mon.core_list[0]
        mon.mon_data_avg[1]["cores"] = mon.core_list[1]
        mon.mon_data[0].values.llc = 1
        mon.mon_data[0].values.llc_misses = 13
        mon.mon_data[0].values.mbm_total = 10
        mon.mon_data[1].values.llc = 100
        mon.mon_data[1].values.llc_misses = 131
        mon.mon_data[1].values.mbm_total = 1011
        mon.store_monitoring_info()
        self.assertEqual(mon.mon_data_avg[0]["llc"]["max"], 1)
        self.assertEqual(mon.mon_data_avg[0]["llc"]["avg"], 1.0)
        self.assertEqual(mon.mon_data_avg[0]["mbm_total"]["max"], 10)
        self.assertEqual(mon.mon_data_avg[0]["mbm_total"]["avg"], 10.0)
        self.assertEqual(mon.mon_data_avg[0]["llc_misses"], 13)
        self.assertEqual(mon.mon_data_avg[1]["llc"]["max"], 100)
        self.assertEqual(mon.mon_data_avg[1]["llc"]["avg"], 100.0)
        self.assertEqual(mon.mon_data_avg[1]["mbm_total"]["max"], 1011)
        self.assertEqual(mon.mon_data_avg[1]["mbm_total"]["avg"], 1011.0)
        self.assertEqual(mon.mon_data_avg[1]["llc_misses"], 131)
        mon.iterations += 1
        mon.mon_data[0].values.llc = 5
        mon.mon_data[1].values.llc = 500
        mon.store_monitoring_info()
        self.assertEqual(mon.mon_data_avg[0]["llc"]["avg"], 3.0)
        self.assertEqual(mon.mon_data_avg[0]["llc"]["max"], 5)
        self.assertEqual(mon.mon_data_avg[1]["llc"]["avg"], 300.0)
        self.assertEqual(mon.mon_data_avg[1]["llc"]["max"], 500)

    def test_mon_poll_monitoring_info(self):
        """
        Test poll_monitoring_info function
        """
        self.mock_pqos.pqos_mon_poll = MagicMock(return_value=Log.PQOS_RETVAL_OK)
        mon = PqosMonitor(self.mock_pqos, False)
        mon.core_list = [[0, 1], [2, 3]]
        mon.mon_data = (CPqosMonData * 2)()
        mon.poll_monitoring_info()

    def test_mon_poll_monitoring_info_error(self):
        """
        Test poll_monitoring_info function for error
        """
        self.mock_pqos.pqos_mon_poll = MagicMock(return_value=Log.PQOS_RETVAL_ERROR)
        mon = PqosMonitor(self.mock_pqos, False)
        mon.core_list = [[0, 1], [2, 3]]
        mon.mon_data = (CPqosMonData * 2)()
        with self.assertRaises(SystemExit) as err:
            mon.poll_monitoring_info()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_mon_poll")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)

    def test_mon_monitor_stop_error(self):
        """
        Test monitor_stop function when pqos_mon_stop function fails
        """
        self.mock_pqos.pqos_mon_stop = MagicMock(return_value=Log.PQOS_RETVAL_ERROR)
        mon = PqosMonitor(self.mock_pqos, False)
        mon.core_list = [[0, 1], [2, 3]]
        mon.mon_data = (CPqosMonData * 2)()

        with self.assertRaises(SystemExit) as err:
            mon.monitor_stop(2, False)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_mon_stop")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)

    def test_mon_monitor_stop(self):
        """
        Test setup_monitoring function
        """
        self.mock_pqos.pqos_mon_stop = MagicMock(return_value=Log.PQOS_RETVAL_OK)
        mon = PqosMonitor(self.mock_pqos, False)
        mon.mon_data = (CPqosMonData * 2)()
        mon.monitor_stop(2, True)
