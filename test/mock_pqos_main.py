# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    This module defines mock pqos library functions for testing main modules
"""
from unittest.mock import Mock, MagicMock
from pqos_wrapper.utils import Log
from test.mock_pqos_alloc import (
    prepare_pqos_alloc_assoc_set,
    prepare_pqos_alloc_assoc_get,
)
from test.mock_pqos_cap import pqos_cap_get_type_mock, pqos_l3ca_get_cos_num_mock
from test.mock_pqos_l3ca import (
    pqos_l3ca_get_min_cbm_bits_mock,
    prepare_l3ca_set,
    pqos_l3ca_get_mock,
)
from test.mock_pqos_cpuinfo import (
    pqos_cpu_get_core_info_mock,
    pqos_cpu_get_sockets_mock,
)


def mock_pqos_lib_basic(libpqos_path):
    """
    mock pqos lib for main module with basic functionality
    """
    mock_pqos = Mock()
    mock_pqos.pqos_init = MagicMock(return_value=Log.PQOS_RETVAL_OK)
    mock_pqos.pqos_alloc_reset = MagicMock(return_value=Log.PQOS_RETVAL_OK)
    mock_pqos.pqos_fini = MagicMock(return_value=Log.PQOS_RETVAL_OK)
    return mock_pqos


def mock_pqos_lib_error_load(libpqos_path):
    """
    mock pqos lib for main module with error in pqos_init
    """
    mock_pqos = Mock()
    mock_pqos.pqos_init = MagicMock(return_value=Log.PQOS_RETVAL_ERROR)
    return mock_pqos


def mock_pqos_lib_error_reset(libpqos_path):
    """
    mock pqos lib for main module with error in pqos_alloc_reset
    """
    mock_pqos = Mock()
    mock_pqos.pqos_init = MagicMock(return_value=Log.PQOS_RETVAL_OK)
    mock_pqos.pqos_alloc_reset = MagicMock(return_value=Log.PQOS_RETVAL_ERROR)
    return mock_pqos


def mock_pqos_lib_error_fini(libpqos_path):
    """
    mock pqos lib for main module with error in pqos_fini
    """
    mock_pqos = Mock()
    mock_pqos.pqos_init = MagicMock(return_value=Log.PQOS_RETVAL_OK)
    mock_pqos.pqos_alloc_reset = MagicMock(return_value=Log.PQOS_RETVAL_OK)
    mock_pqos.pqos_fini = MagicMock(return_value=Log.PQOS_RETVAL_ERROR)
    return mock_pqos


def mock_pqos_lib_integrated(libpqos_path):
    """
    Wrapper function to build all functionalities in mock pqos lib
    """
    mock_pqos = Mock()
    mock_pqos.pqos_init = MagicMock(return_value=Log.PQOS_RETVAL_OK)
    mock_pqos.pqos_alloc_reset = MagicMock(return_value=Log.PQOS_RETVAL_OK)
    mock_pqos.pqos_fini = MagicMock(return_value=Log.PQOS_RETVAL_OK)
    mock_pqos.pqos_cap_get_type = MagicMock(side_effect=pqos_cap_get_type_mock)
    mock_pqos.pqos_l3ca_get_cos_num = MagicMock(side_effect=pqos_l3ca_get_cos_num_mock)
    mock_pqos.pqos_l3ca_get_min_cbm_bits = MagicMock(
        side_effect=pqos_l3ca_get_min_cbm_bits_mock
    )
    mock_pqos.pqos_cpu_get_sockets = MagicMock(side_effect=pqos_cpu_get_sockets_mock)
    mock_pqos.pqos_cpu_get_core_info = MagicMock(
        side_effect=pqos_cpu_get_core_info_mock
    )
    pqos_alloc_assoc_set_mock = prepare_pqos_alloc_assoc_set(8, 2)
    mock_pqos.pqos_alloc_assoc_set = MagicMock(side_effect=pqos_alloc_assoc_set_mock)
    pqos_alloc_assoc_get_mock = prepare_pqos_alloc_assoc_get(8)
    mock_pqos.pqos_alloc_assoc_get = MagicMock(side_effect=pqos_alloc_assoc_get_mock)
    pqos_l3ca_set_mock = prepare_l3ca_set(2, 8, 2)
    mock_pqos.pqos_l3ca_set = MagicMock(side_effect=pqos_l3ca_set_mock)
    mock_pqos.pqos_l3ca_get = MagicMock(side_effect=pqos_l3ca_get_mock)
    return mock_pqos
