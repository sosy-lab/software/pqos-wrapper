# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    This module defines mock pqos library functions and mock PqosCpuInfo, PqosCapability objects for
    testing allocation module
"""
from unittest.mock import Mock, MagicMock
from pqos_wrapper.utils import Log
from test.mock_pqos_cpuinfo import PqosCpuMockBuilder, pqos_cpu_get_sockets_mock
from test.mock_pqos_cap import get_type_mock
from test.mock_pqos_l3ca import (
    prepare_l3ca_set,
    pqos_l3ca_get_min_cbm_bits_mock,
    pqos_l3ca_get_mock,
)
from test.helper import ctypes_ref_set_int


def build_mock_cap_and_pqos(num_cores):
    """
    Build mock PqosCapability and pqos lib objects with cpu info for
    specified num_cores
    """
    mock_cpu = PqosCpuMockBuilder()
    mock_pqos = mock_cpu.build_mock(num_cores)
    mock_cap = Mock()
    return mock_pqos, mock_cap


def prepare_pqos_alloc_assoc_set(num_cores, num_cos):
    """
    Prepare mock pqos_alloc_assoc_set
    """

    def pqos_alloc_assoc_set_mock(core, class_id):
        """
        mock pqos_alloc_assoc_set function
        """
        if class_id >= num_cos or class_id < 0 or core < 0 or core >= num_cores:
            return Log.PQOS_RETVAL_PARAM
        return Log.PQOS_RETVAL_OK

    return pqos_alloc_assoc_set_mock


def prepare_pqos_alloc_assoc_get(num_cores):
    """
    Prepare mock pqos_alloc_assoc_get
    """

    def pqos_alloc_assoc_get_mock(core, class_id_ref):
        """
        mock pqos_alloc_assoc_get function
        """
        if core < 0 or core >= num_cores:
            return Log.PQOS_RETVAL_PARAM
        ctypes_ref_set_int(class_id_ref, 0)
        return Log.PQOS_RETVAL_OK

    return pqos_alloc_assoc_get_mock


def prepare_get_l3ca_info(num_cores, num_classes):
    """
    Prepare mock objects for get_l3ca_info function
    """
    mock_cpu = PqosCpuMockBuilder()
    mock_pqos = mock_cpu.build_mock(num_cores)
    mock_pqos.pqos_cpu_get_sockets = MagicMock(side_effect=pqos_cpu_get_sockets_mock)
    mock_pqos.pqos_l3ca_get_min_cbm_bits = MagicMock(
        side_effect=pqos_l3ca_get_min_cbm_bits_mock
    )
    mock_cap = Mock()
    mock_cap.get_l3ca_cos_num = MagicMock(return_value=num_classes)
    mock_cap.get_type = MagicMock(side_effect=get_type_mock)
    return mock_pqos, mock_cap


def prepare_allocate_resource(num_cores, num_classes, max_bits, min_bits):
    """
    Prepare mock objects for all resource allocation functions
    """
    mock_pqos, mock_cap = prepare_get_l3ca_info(num_cores, num_classes)
    pqos_alloc_assoc_set_mock = prepare_pqos_alloc_assoc_set(num_cores, num_classes)
    mock_pqos.pqos_alloc_assoc_set = MagicMock(side_effect=pqos_alloc_assoc_set_mock)
    pqos_l3ca_set_mock = prepare_l3ca_set(num_classes, max_bits, min_bits)
    mock_pqos.pqos_l3ca_set = MagicMock(side_effect=pqos_l3ca_set_mock)
    return mock_pqos, mock_cap


def prepare_show_association(num_cores, num_classes):
    """
    Prepare mock objects for show association function
    """
    mock_cpu = PqosCpuMockBuilder()
    mock_pqos = mock_cpu.build_mock(num_cores)
    mock_pqos.pqos_cpu_get_sockets = MagicMock(side_effect=pqos_cpu_get_sockets_mock)
    mock_pqos.pqos_l3ca_get = MagicMock(side_effect=pqos_l3ca_get_mock)
    mock_cap = Mock()
    mock_cap.get_l3ca_cos_num = MagicMock(return_value=num_classes)
    return mock_pqos, mock_cap
