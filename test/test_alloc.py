# pqos-wrapper is a wrapper for pqos C library.
# This file is part of pqos-wrapper.
#
# Copyright (C) 2019  Philipp Wendler
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    Unit tests for allocation module
"""
import json
import unittest
from unittest.mock import MagicMock
from pqos_wrapper.allocation import PqosAlloc
from pqos_wrapper.utils import Log
from test.mock_pqos_alloc import (
    build_mock_cap_and_pqos,
    prepare_pqos_alloc_assoc_set,
    prepare_pqos_alloc_assoc_get,
    prepare_get_l3ca_info,
    prepare_show_association,
    prepare_allocate_resource,
)
from test.mock_pqos_cpuinfo import pqos_cpu_get_core_info_mock, PqosCpuMockBuilder


class TestAlloc(unittest.TestCase):
    """
    Testing class for allocation module
    """

    def test_alloc_init(self):
        """
        Test initialisation of PqosAlloc
        """
        mock_pqos, mock_cap = build_mock_cap_and_pqos(8)
        alloc = PqosAlloc(mock_pqos, mock_cap)
        self.assertIsInstance(alloc, PqosAlloc)

    def test_alloc_get_core_info(self):
        """
        Test get_core_info function
        """
        mock_pqos, mock_cap = build_mock_cap_and_pqos(8)
        alloc = PqosAlloc(mock_pqos, mock_cap)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        core_list = [[0, 1], [6, 7]]
        alloc.get_core_info(core_list)
        for index in range(len(core_list)):
            for sub_index in range(len(core_list[index])):
                core = core_list[index][sub_index]
                mock_core_info = PqosCpuMockBuilder.build_core_info(core)
                self.assertEqual(alloc.cores[index][core].core, mock_core_info.lcore)
                self.assertEqual(alloc.cores[index][core].socket, mock_core_info.socket)
                self.assertEqual(alloc.cores[index][core].l3_id, mock_core_info.l3_id)
                self.assertEqual(alloc.cores[index][core].l2_id, mock_core_info.l2_id)

    def test_alloc_get_core_info_error(self):
        """
        Test get_core_info function when incorrect core id is provided
        """
        mock_pqos, mock_cap = build_mock_cap_and_pqos(4)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        alloc = PqosAlloc(mock_pqos, mock_cap)
        core_list = [[0, 1], [6, 7]]
        with self.assertRaises(SystemExit) as err:
            alloc.get_core_info(core_list)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_cpu_get_core_info")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_ERROR)
        self.assertEqual(error_json["error"], True)

    def test_alloc_assoc_set(self):
        """
        Test assoc_set function
        """
        mock_pqos, mock_cap = build_mock_cap_and_pqos(8)
        pqos_alloc_assoc_set_mock = prepare_pqos_alloc_assoc_set(8, 2)
        mock_pqos.pqos_alloc_assoc_set = MagicMock(
            side_effect=pqos_alloc_assoc_set_mock
        )
        alloc = PqosAlloc(mock_pqos, mock_cap)
        alloc.assoc_set(0, 1)
        mock_pqos.pqos_alloc_assoc_set.assert_called_once_with(0, 1)

    def test_alloc_assoc_set_incorrect_cos_error(self):
        """
        Test assoc_set function
        """
        mock_pqos, mock_cap = build_mock_cap_and_pqos(8)
        pqos_alloc_assoc_set_mock = prepare_pqos_alloc_assoc_set(8, 2)
        mock_pqos.pqos_alloc_assoc_set = MagicMock(
            side_effect=pqos_alloc_assoc_set_mock
        )
        alloc = PqosAlloc(mock_pqos, mock_cap)
        with self.assertRaises(SystemExit) as err:
            alloc.assoc_set(0, 4)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_alloc_assoc_set")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_alloc_assoc_set_incorrect_core_error(self):
        """
        Test assoc_set function
        """
        mock_pqos, mock_cap = build_mock_cap_and_pqos(8)
        pqos_alloc_assoc_set_mock = prepare_pqos_alloc_assoc_set(8, 2)
        mock_pqos.pqos_alloc_assoc_set = MagicMock(
            side_effect=pqos_alloc_assoc_set_mock
        )
        alloc = PqosAlloc(mock_pqos, mock_cap)
        with self.assertRaises(SystemExit) as err:
            alloc.assoc_set(10, 0)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_alloc_assoc_set")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_alloc_assoc_get(self):
        """
        Test alloc_get function
        """
        mock_pqos, mock_cap = build_mock_cap_and_pqos(8)
        pqos_alloc_assoc_get_mock = prepare_pqos_alloc_assoc_get(8)
        mock_pqos.pqos_alloc_assoc_get = MagicMock(
            side_effect=pqos_alloc_assoc_get_mock
        )
        alloc = PqosAlloc(mock_pqos, mock_cap)
        class_id = alloc.assoc_get(0)
        self.assertEqual(class_id, 0)

    def test_alloc_assoc_get_error(self):
        """
        Test alloc_get function when incorrect core no is passed
        """
        mock_pqos, mock_cap = build_mock_cap_and_pqos(8)
        pqos_alloc_assoc_get_mock = prepare_pqos_alloc_assoc_get(8)
        mock_pqos.pqos_alloc_assoc_get = MagicMock(
            side_effect=pqos_alloc_assoc_get_mock
        )
        alloc = PqosAlloc(mock_pqos, mock_cap)
        with self.assertRaises(SystemExit) as err:
            alloc.assoc_get(9)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_alloc_assoc_get")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_alloc_show_association(self):
        """
        Test show_association function
        """
        mock_pqos, mock_cap = prepare_show_association(8, 4)
        pqos_alloc_assoc_get_mock = prepare_pqos_alloc_assoc_get(8)
        mock_pqos.pqos_alloc_assoc_get = MagicMock(
            side_effect=pqos_alloc_assoc_get_mock
        )
        alloc = PqosAlloc(mock_pqos, mock_cap)
        output = alloc.show_association()
        for core in range(8):
            self.assertEqual(output["function_output"]["cores"][core], 0)
        for socket in range(2):
            for cos in range(4):
                self.assertEqual(
                    output["function_output"]["sockets"][socket][cos], 65535
                )

    def test_alloc_get_l3ca_info(self):
        """
        Test get_l3ca_info function
        """
        mock_pqos, mock_cap = prepare_get_l3ca_info(8, 4)
        alloc = PqosAlloc(mock_pqos, mock_cap)
        core_list = [[0, 1], [6, 7]]
        alloc.get_core_info(core_list)
        l3ca_info = alloc.get_l3ca_info()
        self.assertEqual(
            l3ca_info["cache_per_run"],
            int(
                mock_cap.get_type("l3ca")["function_output"]["num_ways"]
                // len(core_list)
            ),
        )
        self.assertEqual(
            l3ca_info["cache_bitmask"], (1 << l3ca_info["cache_per_run"]) - 1
        )

    def test_alloc_get_l3ca_info_min_cbm_bits_error(self):
        """
        Check if get_l3ca_info function raises correct error on exceeding
        maximum core set capacity.
        """
        mock_pqos, mock_cap = prepare_get_l3ca_info(8, 4)
        alloc = PqosAlloc(mock_pqos, mock_cap)
        core_list = [[0], [1], [4], [6], [7]]
        alloc.get_core_info(core_list)
        with self.assertRaises(SystemExit) as err:
            alloc.get_l3ca_info()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "get_l3ca_info")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_RESOURCE)
        self.assertEqual(error_json["error"], True)

    def test_alloc_get_l3ca_info_empty_core_list_error(self):
        """
        Check if get_l3ca_info function raises correct error when core
        information is not initialised before execution.
        """
        mock_pqos, mock_cap = prepare_get_l3ca_info(8, 4)
        alloc = PqosAlloc(mock_pqos, mock_cap)
        with self.assertRaises(SystemExit) as err:
            alloc.get_l3ca_info()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "get_l3ca_info")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_RESOURCE)
        self.assertEqual(error_json["error"], True)

    def test_alloc_allocate_l3ca_diff_socket(self):
        """
        Test allocate_l3ca function when core sets are on different sockets
        """
        mock_pqos, mock_cap = prepare_allocate_resource(8, 4, 8, 2)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        core_list = [[0, 1], [6, 7]]
        alloc = PqosAlloc(mock_pqos, mock_cap)
        alloc.get_core_info(core_list)
        alloc.allocate_l3ca()
        self.assertEqual(
            alloc.ret["cache_per_run"],
            int(
                mock_cap.get_type("l3ca")["function_output"]["num_ways"]
                // len(core_list)
            ),
        )
        self.assertEqual(alloc.ret["cores"][0], 0)
        self.assertEqual(alloc.ret["cores"][1], 0)
        self.assertEqual(alloc.ret["cores"][6], 0)
        self.assertEqual(alloc.ret["cores"][7], 0)

        self.assertEqual(alloc.ret["sockets"][0][0], int("00001111", 2))
        self.assertEqual(alloc.ret["sockets"][1][0], int("11110000", 2))

    def test_alloc_allocate_l3ca_same_socket(self):
        """
        Test allocate_l3ca function when core sets are on same sockets
        """
        mock_pqos, mock_cap = prepare_allocate_resource(8, 4, 8, 2)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        core_list = [[0, 1], [2, 3]]
        alloc = PqosAlloc(mock_pqos, mock_cap)
        alloc.get_core_info(core_list)
        alloc.allocate_l3ca()
        self.assertEqual(
            alloc.ret["cache_per_run"],
            int(
                mock_cap.get_type("l3ca")["function_output"]["num_ways"]
                // len(core_list)
            ),
        )
        self.assertEqual(alloc.ret["cores"][0], 0)
        self.assertEqual(alloc.ret["cores"][1], 0)
        self.assertEqual(alloc.ret["cores"][2], 1)
        self.assertEqual(alloc.ret["cores"][3], 1)

        self.assertEqual(alloc.ret["sockets"][0][0], int("00001111", 2))
        self.assertEqual(alloc.ret["sockets"][0][1], int("11110000", 2))

    def test_alloc_allocate_l3ca_error_exceed_cos(self):
        """
        Test allocate_l3ca function when no of runs on a socket exceeds
        the no of COS available
        """
        mock_pqos, mock_cap = prepare_allocate_resource(8, 3, 8, 2)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        core_list = [[0], [1], [2], [3]]
        alloc = PqosAlloc(mock_pqos, mock_cap)
        alloc.get_core_info(core_list)
        with self.assertRaises(SystemExit) as err:
            alloc.allocate_l3ca()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_alloc_assoc_set")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_alloc_allocate_l3ca_error_less_than_min_bits(self):
        """
        Test allocate_l3ca function when cache allocated to a run
        is less than min bits
        """
        mock_pqos, mock_cap = prepare_allocate_resource(8, 3, 8, 4)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        core_list = [[0], [1], [2], [3]]
        alloc = PqosAlloc(mock_pqos, mock_cap)
        alloc.get_core_info(core_list)
        with self.assertRaises(SystemExit) as err:
            alloc.allocate_l3ca()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_l3ca_set")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_alloc_allocate_l3ca_error_more_than_max_bits(self):
        """
        Test allocate_l3ca function when cache allocated to a
        run is more than max bits
        """
        mock_pqos, mock_cap = prepare_allocate_resource(8, 3, 4, 4)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        core_list = [[0]]
        alloc = PqosAlloc(mock_pqos, mock_cap)
        alloc.get_core_info(core_list)
        with self.assertRaises(SystemExit) as err:
            alloc.allocate_l3ca()

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "pqos_l3ca_set")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)

    def test_alloc_allocate_resource(self):
        """
        Test allocate_resource function
        """
        mock_pqos, mock_cap = prepare_allocate_resource(8, 3, 8, 2)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        core_string = "[[0, 1], [2, 3]]"
        alloc = PqosAlloc(mock_pqos, mock_cap)
        ret = alloc.allocate_resource("l3ca", core_string)["function_output"]
        self.assertEqual(ret["cache_per_run"], 4)
        self.assertEqual(ret["cores"][0], 0)
        self.assertEqual(ret["cores"][1], 0)
        self.assertEqual(ret["cores"][2], 1)
        self.assertEqual(ret["cores"][3], 1)

        self.assertEqual(alloc.ret["sockets"][0][0], int("00001111", 2))
        self.assertEqual(alloc.ret["sockets"][0][1], int("11110000", 2))

    def test_alloc_allocate_resource_error(self):
        """
        Test allocate_resource function when non available resource is requested
        """
        mock_pqos, mock_cap = prepare_allocate_resource(8, 3, 8, 2)
        mock_pqos.pqos_cpu_get_core_info = MagicMock(
            side_effect=pqos_cpu_get_core_info_mock
        )
        core_string = "[[0, 1], [2, 3]]"
        alloc = PqosAlloc(mock_pqos, mock_cap)
        with self.assertRaises(SystemExit) as err:
            alloc.allocate_resource("l2ca", core_string)

        error_json = json.loads(str(err.exception))
        self.assertEqual(error_json["function"], "allocate_resource")
        self.assertEqual(error_json["returncode"], Log.PQOS_RETVAL_PARAM)
        self.assertEqual(error_json["error"], True)
