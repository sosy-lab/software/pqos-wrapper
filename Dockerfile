# Docker image with environment for building pqos_wrapper into a single executable
#
# Commands for updating the image:
# docker build --pull -t registry.gitlab.com/sosy-lab/software/pqos-wrapper/build:latest - < Dockerfile
# docker push registry.gitlab.com/sosy-lab/software/pqos-wrapper/build

# Use older Ubuntu to avoid dependencies on newer libc
FROM ubuntu:xenial

RUN apt-get update && apt-get install -y \
  python3-pip

# pip from xenial is too old to install current pyinstaller
RUN pip3 install --upgrade 'pip<21'

RUN pip install \
  pyinstaller
