# pqos-wrapper
The pqos-wrapper is an alternative command-line interface of [Intel PQoS](https://github.com/intel/intel-cmt-cat/tree/master/pqos)
for the purpose of using it in [BenchExec](https://github.com/sosy-lab/benchexec).
It can be used to control cache allocation of benchmark runs
to make performance more deterministic,
and to measure usage of cache and memory bandwidth during benchmarking.
The supported features depends on the CPU of the system
(a modern Intel CPU is required).
For more details please refer to the documentation of
[Intel Resource Director Technology](https://github.com/intel/intel-cmt-cat).

## Installation Instructions
Please first install `libpqos` according to its [documentation](https://github.com/intel/intel-cmt-cat/tree/master/pqos).

To install the cli using `setuptools` use the standard command `sudo python3 setup.py install`.
However, note that in this case only the root user can use of it because of missing capabilities.

The recommended way for installation avoids this and is as follows:

1. Create a group named `msr`.

1. Build the project as an independent executable using `pyinstaller`:
   ```
   pyinstaller --onefile bin/pqos_wrapper 
   ```
   The executable will be built inside a new folder named `dist` with the name `pqos_wrapper`.

   Alternatively, binaries that work on Ubuntu 16.04 or newer (including Debian 10)
   can be downloaded from the artifacts of the build jobs
   of our [continuous integration](https://gitlab.com/sosy-lab/software/pqos-wrapper/pipelines)
   (click on the download icon in the last column of the respective commit).

1. Install the binary in a location such as `/usr/local/bin/pqos_wrapper`

1. Set permissions for all users to use pqos_wrapper:
   ```
   sudo chgrp msr /usr/local/bin/pqos_wrapper
   sudo chmod a+x,g+s /usr/local/bin/pqos_wrapper
   sudo setcap cap_sys_rawio=eip /usr/local/bin/pqos_wrapper
   ```

1. Ensure that group `msr` has read-write permissions to `/dev/cpu/*/msr`,
   e.g., using `sudo chgrp msr /dev/cpu/*/msr; sudo chmod g+rw /dev/cpu/*/msr` after each reboot
   or an appropriate udev rule.

## Command line options
- `-I / --interface`: This option is used to set the interface for initialising the pqos library, the available options are `MSR, OS`. The MSR option is set by default.
- `-v / --verbose`: This option is used to get verbose output from the CLI.
- `-c / --check`: This option is used to check support of a given technology on the system. Available options `l3ca`
- `-s / --show`: This option is used to check core association and COS information about `l3ca`
- `-a / --alloc`: This option is used to allocate resources equally among given sets of cores. This is currently available for `l3ca` only and the the sets of cores are passed to the cli as a 2d list of cores `without spaces`, e.g `[[0,1],[2,3]]`
- `-r / --reset`: This option is used to reset all resources
- `-m / --monitor`: This option is used to monitor events for given sets of cores. The sets of cores are passed to the cli as shown in option above.
- `-o / --output`: This option is used to view live output of the monitoring cli.
- `-t / --time-interval`: This option is used to set time interval between two calls for polling monitoring data.
- `-rm / --resetmon`: This option is used to reset monitoring by setting RMID 0 for all cores 

## Usage examples
- To set `OS` interface: `pqos_wrapper -I`
- To set verbose output: `pqos_wrapper -v`
- To reset all resources: `pqos_wrapper -r`
- To check support of `l3ca` on the system: `pqos_wrapper -c l3ca`
- To check core and COS association for `l3ca` on the system: `pqos_wrapper -s`
- To allocate `l3 cache` equally among two sets of cores: `pqos_wrapper -a l3ca [[0,1],[2,3]]`
- To monitor events for sets of cores: `pqos_wrapper -m [[0,1],[2,3]]`
- To monitor events for sets of cores with live output: `pqos_wrapper -m [[0,1],[2,3]] -o`
- To monitor events for sets of cores with custom time interval: `pqos_wrapper -m [[0,1],[2,3]] -t 0.001`
- To reset monitoring: `pqos_wrapper -rm`